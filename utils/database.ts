import mongoose from "mongoose";

const DB_URI = process.env.MONGO_URI || "";

const MONGOOSE_OPTIONS: mongoose.ConnectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  keepAlive: true,
  autoReconnect: true,
  reconnectTries: 3,
  reconnectInterval: 5000,
  useFindAndModify: false,
};

const connection = {
  isConnected: 0,
};

const dbConnect = async () => {
  if (connection.isConnected) {
    return;
  }

  const db = await mongoose.connect(DB_URI, MONGOOSE_OPTIONS);
  connection.isConnected = db.connections[0].readyState;
};

export default dbConnect;
