import { Content, Course, User } from "../interfaces";

const HOST = process.env.HOST;

const REQUEST_CONFIG = {
  headers: {
    contentType: "application/json",
    accept: "application/json",
  },
};

export const fetchAllCoursesData: () => Promise<Course[]> = async () => {
  const response = await fetch(HOST + "/api/courses", REQUEST_CONFIG);
  return response.json();
};

export const fetchPublicContentsData: () => Promise<Content[]> = async () => {
  const response = await fetch(HOST + "/api/contents/public", REQUEST_CONFIG);
  return response.json();
};

export const fetchAllContentsData: () => Promise<Content[]> = async () => {
  const response = await fetch(HOST + "/api/contents", REQUEST_CONFIG);
  return response.json();
};

export const fetchContentById: (id: string) => Promise<Content> = async (
  id: string
) => {
  const response = await fetch(HOST + `/api/contents/${id}`, REQUEST_CONFIG);
  return response.json();
};

export const fetchUserById: (id: string) => Promise<User> = async (
  id: string
) => {
  const response = await fetch(HOST + `/api/users/${id}`, REQUEST_CONFIG);
  return response.json();
};

export const fetchContentsByAuthor: (id: string) => Promise<Content[]> = async (
  id: string
) => {
  const response = await fetch(
    HOST + `/api/users/${id}/contents`,
    REQUEST_CONFIG
  );
  return response.json();
};

export const fetchCoursesByAuthor: (id: string) => Promise<Course[]> = async (
  id: string
) => {
  const response = await fetch(
    HOST + `/api/users/${id}/couurses`,
    REQUEST_CONFIG
  );
  return response.json();
};
