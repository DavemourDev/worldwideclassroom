export const encodeBase64 = async (
  file: File,
  callback: (result: string | ArrayBuffer | null) => void
) => {
  const imageReader = new FileReader();
  imageReader.onloadend = () => {
    callback(imageReader.result);
  };
  imageReader.readAsDataURL(file);
};
