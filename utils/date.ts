export const localeDate = (date: string | number) => {
  return new Date(date).toLocaleDateString();
};
