export type LanguageType = "es" | "ca" | "en";

export type DictionaryType = {
  teacherLabel: string;
  studentLabel: string;
  contentLabel: string;
  courseLabel: string;

  descriptionLabel: string;
  titleLabel: string;
  visibilityLabel: string;
  publicLabel: string;
  privateLabel: string;
  aboutContent: string;
  aboutNavLabel: string;
  exploreNavLabel: string;
  loginNavLabel: string;
  registerNavLabel: string;
  usernameLabel: string;
  passwordLabel: string;
  emptyDescription: string;
  landingPageHeading: string;
  landingPageSection1: string;
  landingPageSection2: string;
  landingPageSection3: string;
  saveContentLabel: string;
  saveCourseLabel: string;
  roleLabel: string;
  fullNameLabel: string;
  createTextLabel: string;
  createQuizLabel: string;
  logoutLabel: string;
  myContentsLabel: string;
  myCoursesLabel: string;
  coursesLabel: string;
  contentsLabel: string;
  textContentLabel: string;
  formContentLabel: string;
  noContentsMessage: string;
  noCoursesMessage: string;
  createdBy: string;
  searchByTitleLabel: string;
  createCourseLabel: string;
  editDescriptionLabel: string;
  successChangeDescriptionMessage: string;
};

export const DICTIONARY_ES: DictionaryType = {
  teacherLabel: "Profesor/a",
  studentLabel: "Estudiante",
  contentLabel: "Contenido",
  courseLabel: "Curso",
  titleLabel: "Título",
  fullNameLabel: "Nombre completo",
  visibilityLabel: "Visibilidad",
  descriptionLabel: "Descripción",
  aboutContent: `## SOBRE WWCR\nWWCR es una plataforma didáctica que conecta profesorado y alumnado mediante un repositorio de materiales y recursos colaborativos sobre diferentes disciplinas que además permite hacer un seguimiento de los cursos ofertados.\nNuestro objetivo es poner al alcance de toda la comunidad educativa materiales de interés desde un único punto, así como proveer de las herramientas necesarias para un buen uso de éstos.\n## Sobre el creador\n### David M. Ureña\nSoy desarrollador web y profesor de matemáticas, y como partícipe del sistema educativo, soy consciente de los problemas y limitaciones del mismo a la hora de plantear el aprendizaje de las matemáticas como una herramienta para la resolución de problemas y para potenciar el pensamiento abstracto, en lugar de ser meramente un filtro por el que hay que pasar para graduarse.\nEste proyecto nace de la necesidad de un entorno colaborativo en el que los profesores puedan subir sus contenidos de forma abierta, ya no solamente de matemáticas,sino de cualquier materia, de forma que sean accesibles y extensibles por toda la comunidad. \n`,
  aboutNavLabel: "Acerca de",
  exploreNavLabel: "Explora",
  loginNavLabel: "Iniciar sesión",
  registerNavLabel: "Registrarse",
  usernameLabel: "Nombre de usuario",
  passwordLabel: "Contraseña",
  emptyDescription: "Descripción vacía",
  saveContentLabel: "Guardar contenido",
  landingPageHeading:
    "# ¡Bienvenid@ a WorldWide ClassRoom!\n## ¡Un entorno para las comunidades educativas de todo el mundo!",
  landingPageSection1:
    "## ¡Conecta con cualquiera desde cualquier lugar!\n\nPuedes acceder a contenidos de profesores alrededor de todo el globo.\n\nPuedes crear tus materiales para ayudar a estudiantes de todo el mundo.",

  landingPageSection2:
    "## Extiende contenidos de otros profesores!\nCada profesor/profesora tiene su propio punto de vista!\n\nPuedes ofrecer el tuyo propio para mejorar los contenidos o hacerlos más completos!",
  publicLabel: "Público",
  privateLabel: "Privado",
  landingPageSection3:
    "## Crea tus propios cursos o únete a los de otros!\nComo profesor/a, puedes crear tus propios cursos o colaborar con otros profesores!\n\nComo estudiante, te puedes matricular en cursos para labrar tu propio camino en el aprendizaje!",
  roleLabel: "Rol de usuario",

  createTextLabel: "Crear texto",
  createQuizLabel: "Crear cuestionario",

  logoutLabel: "Cerrar sesión",
  myContentsLabel: "Mis contenidos",
  myCoursesLabel: "Mis cursos",

  coursesLabel: "Cursos",
  contentsLabel: "Contenidos",

  textContentLabel: "Texto",
  formContentLabel: "Cuestionario",
  noContentsMessage: "No hay contenidos para mostrar...",
  noCoursesMessage: "No hay cursos para mostrar...",
  createdBy: "Creado por",
  searchByTitleLabel: "Buscar por título",
  saveCourseLabel: "Guardar curso",
  createCourseLabel: "Crear curso",
  editDescriptionLabel: "Editar descripción",
  successChangeDescriptionMessage: "Descripción editada con éxito",
};
export const DICTIONARY_CA: DictionaryType = {
  teacherLabel: "Professor/a",
  studentLabel: "Estudiant",
  contentLabel: "Contingut",
  courseLabel: "Curs",
  titleLabel: "Títol",
  fullNameLabel: "Nom complet",
  visibilityLabel: "Visibilitat",
  descriptionLabel: "Descripció",
  aboutContent: `## SOBRE WWCR\nWWCR és una plataforma didàctica que connecta professorat i alumnat mitjançant un repositori de materials i recursos col·laboratius sobre diferents disciplines.\nEl nostre objectiu és posar a l'abast de tota la comunitat educativa materials d'interés des d'un únic punt, així com proveïr de les eines necessàries per a un bon ús d'aquests.\n## Sobre el creador\n### David M. Ureña\nSoc desenvolupador web i professor de matemàtiques, i com a participant del sistema educatiu, soc conscient dels problemes i limitacions del mateix a l'hora de plantejar l'aprenentatge de les matemàtiques. como una herramienta para la resolución de problemas y para potenciar el pensamiento abstracto, en lugar de ser meramente un filtro por el que hay que pasar para graduarse.\nEste proyecto nace de la necesidad de un entorno colaborativo en el que los profesores puedan subir sus contenidos de forma abierta, ya no solamente de matemáticas,sino de cualquier materia, de forma que sean accesibles y extensibles por toda la comunidad. \n`,
  aboutNavLabel: "Sobre...",
  exploreNavLabel: "Explora",
  loginNavLabel: "Inicia sessió",
  registerNavLabel: "Registra't",
  usernameLabel: "Nom d'usuari",
  passwordLabel: "Contrassenya",
  emptyDescription: "Descripció buida",
  saveContentLabel: "Desar contingut",
  landingPageHeading:
    "# Benvingut/da a WorldWide ClassRoom!\n## Un entorn per a les comunitats educatives arran el món!",
  landingPageSection1:
    "## Connecta amb qualsevol des de qualsevol lloc!\n\nPots accedir a continguts de professors arran de tot el món.\n\nPots crear els teus propis materials i posar-los a l'abast d'estudiants de tot el món.",

  landingPageSection2:
    "## Extén continguts d'altres professors/professores!\n\nCada professor/professora té el seu propi punt de vista!\n\nPots oferit el teu per tal de millorar continguts o fer-los més complets!",
  publicLabel: "Públic",
  privateLabel: "Privat",
  landingPageSection3:
    "## Crea els teus propis cursos o uneix-te als d'altres!\n\nCom a professor/a, pots crear els teus propis cursos o bé col·laborar amb altres professors/es!\n\nCom a estudiant, et pots matricular en cursos per tal de tenir el teu propi camí en l'aprenentatge!",
  roleLabel: "Rol d'usuari",

  createTextLabel: "Crear text",
  createQuizLabel: "Crear qüestionari",
  logoutLabel: "Tancar sessió",
  myContentsLabel: "Els meus continguts",
  myCoursesLabel: "Els meus cursos",

  coursesLabel: "Cursos",
  contentsLabel: "Continguts",

  textContentLabel: "Text",
  formContentLabel: "Qüestionari",
  noContentsMessage: "No hi ha continguts per a mostrar...",
  noCoursesMessage: "No hi ha cursos per a mostrar...",
  createdBy: "Creat per",
  searchByTitleLabel: "Cercar per títol",
  saveCourseLabel: "Desar curs",
  createCourseLabel: "Crear curs",
  editDescriptionLabel: "Edita descripció",
  successChangeDescriptionMessage: "Descripció editada amb èxit",
};

export const DICTIONARY_EN: DictionaryType = {
  teacherLabel: "Teacher",
  studentLabel: "Student",
  contentLabel: "Content",
  courseLabel: "Course",
  titleLabel: "Title",
  visibilityLabel: "Visibility",
  descriptionLabel: "description",
  saveContentLabel: "Save content",
  aboutContent: `## ABOUT WWCR\n\nWWCR is an educational platform connecting teachers and students on an educational resources collaborative repository, about a great variety of subjects.\nOur objective is put on reach of all educative communities a wide set of interesting contents from a single point.\n\n## About the creator\n### David M. Ureña\nI'm a web developer and math teacher. As participant on educational system, I'm conscious on problems and limitations about math learning as a tool for solving problems and train abstrct thinking, rather than a mere filter on the way to graduation.\nThis project borns from the needs of a collaborative environment where teachers can upload contents un an open way, either maths,    either any other subject, making them accessible for everyone.\n`,
  aboutNavLabel: "About",
  exploreNavLabel: "Explore",
  loginNavLabel: "Login",
  registerNavLabel: "Register",
  usernameLabel: "Username",
  passwordLabel: "Password",
  fullNameLabel: "Full name",
  emptyDescription: "Empty description",
  landingPageHeading:
    "# Welcome to WorldWide ClassRoom!\n## An environment for educational communities around the world!",
  landingPageSection1:
    "## Connect with anyone from anywhere!\n\nYou can access contents from teachers around the globe.\n\nYou can provide your stuff for helping students around the world!",
  landingPageSection2:
    "## Extend contents created by other teachers!\nEvery teacher has his/her own point of view\n\nYou can offer yours for helping to improve contents or making them more complete!",
  landingPageSection3:
    "## Make your own courses or join others!\n\nAs a teacher, you can create a course or collaborate with other teachers for that!\n\nAs a student you can enroll courses for having a learning path!",
  publicLabel: "Public",
  privateLabel: "Private",
  roleLabel: "User role",

  createTextLabel: "Create text",
  createQuizLabel: "Create quiz",
  logoutLabel: "Logout",
  myContentsLabel: "My contents",
  myCoursesLabel: "My courses",

  coursesLabel: "Courses",
  contentsLabel: "Contents",

  textContentLabel: "Text",
  formContentLabel: "Quiz",

  noContentsMessage: "No contents to show...",
  noCoursesMessage: "No courses to show...",
  createdBy: "Created by",

  searchByTitleLabel: "Search by title",
  saveCourseLabel: "Save course",
  createCourseLabel: "Create course",
  editDescriptionLabel: "Edit description",

  successChangeDescriptionMessage: "Description successfully changed",
};

export const getLanguageDictionary = (language: LanguageType) => {
  switch (language) {
    case "es":
      return DICTIONARY_ES;
    case "ca":
      return DICTIONARY_CA;
    case "en":
    default:
      return DICTIONARY_EN;
  }
};
