import mongoose, { Document, Model, Schema } from "mongoose";
import { v4 as genID } from "uuid";
import { Content } from "../interfaces";

const TITLE_MAX_LENGTH = 40;

const SCHEMA_OPTIONS = {
  //  discriminatorKey: "type",
  timestamps: {
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
};

interface IContent extends Document {
  // Instance schema methods here
}

interface ContentModel extends Model<IContent> {
  // Static schema methods here
  findAllPublic: () => Promise<Content[]>;
  findByAuthorId: (id: string) => Promise<Content[]>;
}

const ContentSchema = new Schema(
  {
    id: {
      type: String,
    },
    title: {
      type: String,
      required: [true, "Content needs a title"],
      trim: true,
      maxLength: [
        TITLE_MAX_LENGTH,
        `Content title cannot exceed ${TITLE_MAX_LENGTH} characters`,
      ],
    },
    type: {
      type: String,
      enum: ["text", "form"],
      required: [true, "Content needs a type"],
      trim: true,
    },
    isPublic: {
      type: Boolean,
      required: [true],
    },
    author: {
      type: String,
      ref: "User",
    },
    content: {
      type: Object,
      required: [true, "Content is required"],
    },
  },
  SCHEMA_OPTIONS
);

ContentSchema.pre("save", function (next) {
  const content = this;

  if (content.isNew) {
    content.set("id", genID());
  }

  return next();
});

ContentSchema.static("findAllPublic", async function () {
  return this.find({ isPublic: true });
});

ContentSchema.static("findByAuthorId", async function (authorID) {
  return this.find({ author: authorID });
});

export default mongoose.model<IContent, ContentModel>("Content", ContentSchema);
