import mongoose, { Schema, Document, Model } from "mongoose";
import bcrypt from "bcrypt";
import { v4 as genID } from "uuid";
import { User } from "../interfaces";

const MIN_USERNAME_LENGTH = 3;
const MAX_USERNAME_LENGTH = 40;
const MIN_PASSWORD_LENGTH = 8;
const MAX_PASSWORD_LENGTH = 255;
const MAX_DESCRIPTION_LENGTH = 255;

const N_SALTS = 10;

interface IUser extends Document {
  username: string;
  password: string;
  // Instance schema methods here
  comparePassword: (
    password: string,
    callback: (error: Error, isMatching: boolean) => void
  ) => void;
}

interface UserModel extends Model<IUser> {
  // Static schema methods here
  findAllStudents: () => Promise<User[]>;
  findAllTeachers: () => Promise<User[]>;
}

const UserSchema = new Schema(
  {
    id: {
      type: String,
      unique: true,
    },
    username: {
      type: String,
      required: [true, "User needs an username"],
      unique: true,
      trim: true,
      minLength: [
        MIN_USERNAME_LENGTH,
        `Username needs a minimum of ${MIN_USERNAME_LENGTH} characters`,
      ],
      maxLength: [
        MAX_USERNAME_LENGTH,
        `Username cannot exceed ${MAX_USERNAME_LENGTH} characters`,
      ],
    },
    password: {
      type: String,
      required: [true, "User requires a password"],
      trim: true,
      minLength: [
        MIN_PASSWORD_LENGTH,
        `Password needs a minimum of ${MIN_PASSWORD_LENGTH} characters`,
      ],
      maxLength: [
        MAX_PASSWORD_LENGTH,
        `Password cannot exceed ${MAX_PASSWORD_LENGTH} characters`,
      ],
    },
    fullName: {
      type: String,
      required: [true, "User requires a full name"],
      trim: true,
    },
    picture: {
      type: String,
      required: [true, "User requires a picture"],
      trim: true,
    },
    role: {
      type: String,
      enum: ["teacher", "student"],
      required: [true, "User requires a role"],
      trim: true,
    },
    description: {
      type: String,
      maxLength: [
        MAX_DESCRIPTION_LENGTH,
        "User description cannot exceed 255 characters",
      ],
      trim: true,
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

// Antes de grabar el usuario en la base de datos, se codifica un hash de la contraseña.

UserSchema.pre("save", function (next) {
  const user = this;

  if (user.isNew) {
    // No confundir con __id generado por Mongo.
    user.set("id", genID());
  }

  if (user.isModified("password") || user.isNew) {
    bcrypt.genSalt(N_SALTS, function (err, salt) {
      if (err) {
        return next(err);
      }

      bcrypt.hash(user.get("password"), salt, function (err, hash) {
        if (err) {
          return next(err);
        }

        user.set("password", hash);
        return next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function (password, callback) {
  bcrypt.compare(password, this.get("password"), function (error, isMatching) {
    if (error) return callback(error);
    callback(null, isMatching);
  });
};

UserSchema.static("findAllTeachers", async function () {
  return this.find({ role: "teacher" });
});

UserSchema.static("findAllStudents", async function () {
  return this.find({ role: "student" });
});

export default mongoose.model<IUser, UserModel>("User", UserSchema);
