import mongoose, { Model, Schema, Document } from "mongoose";
import { v4 as genID } from "uuid";
import { Course } from "../interfaces";

interface ICourse extends Document {
  // Instance schema methods here
  // getContents: () => Promise<Content[]>;
}

interface CourseModel extends Model<ICourse> {
  // Static schema methods here
  findByAuthorId: (id: string) => Promise<Course[]>;
}

const CourseSchema = new Schema(
  {
    id: {
      type: String,
      unique: true,
    },
    title: {
      type: String,
      required: [true, "Course needs a title"],
      unique: [true, "Course title must be unique"],
      trim: true,
      maxLength: [40, "Course title cannot exceed 40 characters"],
    },
    description: {
      type: String,
      trim: true,
      maxLength: [255, "Course description cannot exceed 255 characters"],
    },
    author: {
      type: String,
      ref: "User",
    },
    participants: {
      type: [String],
      ref: "User",
    },
    enrolledStudents: {
      type: [String],
      ref: "User",
    },
    contents: {
      type: [String],
      ref: "Content",
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

CourseSchema.pre("save", function (next) {
  const course = this;

  if (course.isNew) {
    course.set("id", genID());
  }

  return next();
});

CourseSchema.static("findByAuthorId", function (authorID) {
  return this.find({ author: authorID });
});

export default mongoose.model<ICourse, CourseModel>("Course", CourseSchema);
