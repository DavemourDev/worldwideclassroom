# Documentación proyecto


## Introducción

Este proyecto nace de la necesidad de un entorno abierto para el aprendizaje y la enseñanza.

A simple vista, puede entenderse como un campus virtual. Como una especie de "moodle". Como un repositorio de apuntes. Como una plataforma de cursos. Una forma de organizar formaciones independientemente de su nivel de formalidad. Una herramienta comunicativa entre profesores y alumnos. Un entorno colaborativo para la comunidad educativa.

WorldWide Classroom pretende ser todo eso y más: Una plataforma todo en uno para estudiantes y profesores. Muy sencilla, pero a la vez con muchísimas posibilidades, utilizable desde la mayoría de dispositivos.

En esta presentación, correspondiente a la fase actual del proyecto, las funcionalidades presentadas e implementadas son pocas, pero considero que son suficientemente interesantes como para poder constituir las bases de un proyecto con mucha vista hacia el futuro.

## Organización

- He utilizado `Trello` como punto de apoyo para establecer las tareas a realizar.
- Al ser yo solo el único integrante del grupo, me he tomado la licencia también de utilizar una pizarra para organizar ideas y estructuras.

## Tech stack

### IDE: VSCode
Para el desarrollo de código, he utilizado el editor Visual Studio Code, ya que está muy orientado a trabajar con javascript (el lenguaje principal de las tecnologías que formaban parte del proyecto) y con las múltiples stacks de que dispone, y permite integrar multitud de herramientas de forma rápida y sencilla. Sin duda, un IDE que equilibra perfectamente potencia con velocidad.

### Control de versiones: GitLab

GitLab es un repositorio remoto basado en Git. Utilizado no solamente como repositorio de versiones sino para integración continua.

### Base de datos: MongoDB, alojado en Mongo Atlas, administrado a través de Mongo Compass

MongoDB es un sistema gestor de bases de datos basado en documentos. A diferencia de los sistemas relacionales, la estructura de los datos permite muchísima flexibilidad, pudiendo adecuar de una forma más natural el contenido de los mismos.

Mongo por sí solo no tiene restricciones en la estructura de datos, sino que para ello depende de utilizar un ORM en las aplicaciones intermedias que establezcan un esquema. Para establecer el modelo de datos y el esquema, he utilizado Mongoose, el cual se integra perfectamente en el ecosistema de Node. 

Además, el lenguaje para realizar las consultas es muy sencillo y próximo a JavaScript (de hecho, los datos se guardan en formato BSON, "JSON" binario).

Para administrar la base de datos he utilizado Mongo Compass, un cliente de Mongo en escritorio muy potente a la par que fácil de utilizar.

### Lenguaje: Typescript

Javascript ha evolucionado muchísimo, desde los tiempos en que solamente era un "lenguaje de juguete" para mover cosas en la interfaz gráfica. Ha crecido al punto que se utiliza en servidor, gracias a Node y el motor V8. ¡Quién lo hubiera dicho! 
No obstante, siguen existiendo una serie de carencias que lo hacen difícil de utilizar:
- Tipado muy débil, incluso ambigüo. Además, nada te impide modificar los tipos a tu antojo, y es difícil manejar y tener controlados dichos cambios.
- No todos los navegadores soportan las nuevas versiones del estándar ECMAScript en que se basa javascript.
- El código que se puede escribir en las versiones antiguas a veces es muy críptico, la orientación a objetos basada en prototipos es muy difícil de comprender, a pesar de lo potente que es.
- Es difícil establecer convenciones con todos estos contras.
- A pesar de tener en las últimas versiones alias que camuflan los prototipos y los hacen pasar por clases para el programador, carece de una implementación completa de muchos conceptos de las mismas: la encapsulación como tal no existe (de momento), no hay mecanismos de herencia múltiple obvios...

Typescript ha llegado para poder resolver todos estos problemas, siendo un superconjunto de Javascript (cualquier código javascript es código Typescript válido).

Incluye una serie de características (tipos, enumeraciones, interfaces, miembros de clase privados, autoinicializadores de propiedades en constructor) que facilitan muchísimo la escritura de código javascript.

¿Qué podemos decir de él?

- El código Typescript no se ejecuta directamente, sino que se "transpila" a una versión de ECMAScript objetivo (permitiendo así escribir código moderno que pueda ejecutarse en cualquier entorno JS, sin necesidad de escribir polyfills ni preocuparse por nada).

- Existe una herramienta llamada Quokka, la cual he utilizado con VSCode, para poder evaluar los tipos de las variables ¡En cada línea de código! Esto permite detectar inconsistencias, dadas, por ejemplo, con el tipo  `never` (es el tipo que asigna TS a una variable que no esté garantizado que pueda ser asignada en una sentencia).

- Al igual que Javascript, con el que se puede escribir JSX, Typescript permite la escritura de TSX (permitiendo así usar React con todos los beneficios que aporta TypeScript).

### Framework: Next.js - El React con servidor

Como solución para desarrollar cliente web, personalmente me he enamorado de todo lo que ofrece React como librería. No obstante, falta la parte del servidor. "¿Y si existiera una forma de generar una aplicación web con React que de alguna forma ligara cliente con servidor?".

Sí que es cierto que hasta cierto punto conviene una separación entre ambos entornos, pero cuando compartimos el mismo modelo de datos en ambos entornos, tener que replicarlos en ambos lados de la aplicación es tedioso y genera inconsistencias.

¿Qué tal si el servidor generase código de React ejecutable en el cliente, y si antes de ser enviado ejecutase código en el servidor para adecuarlo?

Next.js fue desarrollado por Vercel para ese cometido, permitiendo también integrar muchísimas facilidades para integrar otras herramientas, entre ellas entorno Typescript.

Asimismo, todas las rutas parten de la base del directorio `/pages`, permitiendo generar fácilmente el mapa de rutas del sitio y una API REST.

Asimismo, Next.js permite integrar casi cualquier cosa que no integre nativamente pero que sea integrable en Express.js, como middlewares, bases de datos, GraphQL, etc.

#### Front-end: React-Next

El primer "framework" que vaig utilitzar per al front-end va ser Angular, el qual vaig aprendre a utilitzar a un curs i en vaig profunditzar a l'empresa de pràctiques. 

És una eina molt potent, no obstant, és molt difícil de dominar, ja que engloba una documentació i uns conceptes/utilitats molt extensos.

A banda, tot el projecte (o al menys el front-end) és obligat a una estructura de projecte determinada i complexa.

React, tot i no oferir les mateixes possibilitats per si sol (no es més que una llibreria orientada a components, més que un framework en si mateix), ho fa fàcil, modular, i molt personalitzable.

Next aprofita aquest enfoc per a crear les pàgines basant-se en aquest principi, i afegint la possibilitat de generar les dades com a "props" (arguments) dels components que les constitueixen, així com mapejar els paràmetres de les peticions, d'una forma bastant intuitiva.

React ha evolucionat en una direcció de simplicitat, amb els components funcionals i els hooks (que en Angular s'implementarien com a `services`), que al final no son més que funcions que poden utilitzar característiques de components `react`, com estats o elements de contextes.

#### Back-end: Ecosistema de servidor de next, como API REST y con Mongoose como modelo

Mi idea inicial era implementar la API como un GraphQL, de forma que podría realizar las consultas de una forma muy flexible, ya que este tipo de aplicación tiene un modelo de datos muy pequeño, pero con muchísimas relaciones entre sus elementos.

Sin embargo, tuve problemas implementando los mutadores, y además tenía que crear un esquema específicamente para graphql utilizando sus herramientas. Esto hace tres esquemas de modelo en mi aplicación: El de mongoose, el de graphql y la interfaz de tipado de TS.

Sin duda, para la forma en que estaba organizado, no era práctico, por tanto me enfoqué en la API rest.

La ventaja de Next es que trae una implementación de la función `fetch` para servidor y para cliente, con la misma interfaz para ambos entornos. 

Además, los `handlers` de la API (similares a los servlets en java) funcionan indistintamente para ejecución síncrona y asíncrona, de forma que utilizar una API REST de toda la vida tampoco ha sido tan doloroso, después de todo.

### Autenticación: La simpleza de jwt

Para la autenticación de usuarios, he utilizado la librería `jsonwebtoken`, la cual permite generar una token de autenticación que almacena información encriptada del usuario y se puede utilizar como clave de autorización para los diferetes servicios de la aplicación.

## Análisis funcional

Por ahora solamente se han implementado las funcionalidades del profesor, así como un panel de exploración.

- El registro permite registrarse como profesor o como estudiante. 
  - Por ahora, para una persona es necesario tener una cuenta para cada uno de los dos roles si quiere desarrollar ambos en la plataforma. 
  - Este problema se resolverá mediante una distinción de perfiles de cada usuario.

- El panel de exploración es visible por cualquier usuario, incluso aquellos no logueados, y permite visualizar y filtrar todos aquellos contenidos y cursos que sean públicos.

- Los profesores pueden crear contenidos de dos tipos: texto y test.
  - Los contenidos de texto se presentan en formato `markdown`, y muestran una vista previa del markdown que está siendo escrito, así como una interfaz para crear fácilmente elementos en markdown (tablas, listas, formatos...) sin necesidad de conocer markdown en profindidad.
- Los profesores pueden crear cursos. Por ahora se añaden detalles mínimos en ellos.
  - En un futuro, se permitirá agregar contenidos a los cursos, así como atender solicitudes de matrícula por parte de alumnos y de colaboración por parte de otros profesores.
  - Los contenidos podrán desenvocar en tareas para los estudiantes matriculados en el curso. Dichas tareas podrán ser evaluadas.

## Diagramas

### Modelo de datos

Al ser Mongo un sistema gestor de base de datos no relacional y flexible basado en documentos, la forma en que está estructurada la base de datos es similar a la del modelo de clases de la aplicación. Los objetos que se obtienen de la base de datos comparten interfaz con los del modelo.

Se puede ver la estructura en el documento adjunto:
[./model.uxf]()


Debido a la complejidad de relaciones posibles, éstas se explican en:
[./desarrollo-model.md]()

## Enlaces
[https://gitlab.com/DavemourDev/worldwideclassroom]
