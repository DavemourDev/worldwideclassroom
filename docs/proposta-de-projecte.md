# Proposta de projecte

## IMPORTANT!
- El projecte forma part del mòdul integrat de FCT, per la qual cosa, es permet fer-ho individualment i la seva duració serà de 220h.

## Nom del projecte

WorldWide ClassRoom

## Alumnes

- David Moraño Ureña

## Tipus d'aplicació

Aplicació web (Client + servidor)
Base de dades com a servei distribuït

## Descripció bàsica del projecte

El projecte essencialment consisteix en un repositori de material didàctic de diverses disciplines, amb l'afegit de ser una plataforma a través de la qual es pot portar un seguiment de cursos entre alumnes i professors.

La plataforma presentarà microaplicacions addicionals amb les quals es pretén treballar amb aquests continguts d'una forma flexible.

La finalitat és posar a l'abast de tota la comunitat educativa materials d'interés accessibles des d'un únic punt, així com proveïr de les eines necessàries per a fer un bon ús d'aquests.

## Anàlisi no funcional

### Eines de desenvolupament

- IDE: Visual Studio Code.
- Framework: Next.js
- Front-end: React (a través de Next.js)
- Back-end: API GraphQL amb Nodejs (a través de Next.js)
- Llenguatge de programació: Typescript
- Capa de persistència: MongoDB (La naturalesa del model requereix estructures de dades flexibles i operacions de replicació d'informació, per tant un model relacional no és pràctic). Aquesta capa des del servidor serà accedida utilitzant la llibreria Mongoose.
- Repositori Git: GitLab
- Organització: Trello.

### Motivacions del projecte

A part d'estudiar el cicle, treballo com a professor a una acadèmia. En aquest temps, més que mai, és molt valorable disposar de materials en línia per tal de suportar els continguts impartits. Gairebé tots els centres d'estudis disposen de plataformes per a monitoritzar el seguiment del curs acadèmic, però moltes vegades l'accés als continguts és volàtil i no està centralitzat (els alumnes, sobretot, no disposen de facilitats òbvies per a disposar d'aquests materials lliurement o accedir a ells còmodament des de qualsevol dispositiu). Tanmateix, moltes vegades els professors no tenen un medi estandaritzat a través del qual compartir apunts, propis o de tercers.
La informació, especialment en aquest cas en l'àmbit de l'educació, ha de ser accessible a tothom, i aquest projecte neix com una proposta per a fer-ho més possible.

### Sobre les eines utilitzades

Durant l'últim any he treballat en projectes personals utilitzant l'stack de tecnologies que proveeix Next.js. 

Al projecte final realitzat anteriorment, per al servidor he utilitzat l'enfoc REST. En aquest cas, vull explorar una opció relativament novedosa, amb molt de futur i un gran potencial: GraphQL.
Tinc previst dedicar una setmana a l'aprenentatge d'aquesta tecnologia, mitjançant el desenvolupament de la API del model de dades del projecte.

També vull perfeccionar les meves habilitats amb React, llibreria front-end que utilitza Next.js, i que actualment té un papel molt important al mercat.

Per a Next.js utilitzaré TypeScript com a llenguatge de programació, ja que proveeix a JavaScript de propietats com el tipat i estructures de dades més consistents, així com una major facilitat per al testeig i l'aplicació de bones pràctiques.

Per als estils, utilitzaré els mòduls scss, ja que considero que són suficients per a les exigències donades per el projecte a nivell d'interfaç d'usuari en un ecosistema orientat a components.

## Anàlisi funcional

### Rols d'usuari 
L'aplicació tindrà els següents rols d'usuari.
- Guest: Pot veure els continguts públics i interactuar amb ells (llegir + fer test).
- Alumne: Pot veure els continguts públics i interactuar amb ells (llegir + fer test + "enllaçar"), i a més pot ser inscrit en cursos i accedir a tots els seus continguts autoritzats.
- Professor: Pot veure els continguts públics i els seus continguts privats, i a més pot crear continguts públics o privats, així com cursos, i afegir o sustraure continguts i alumnes d'ells, així com evaluar.
 - Administrador: Control total sobre els continguts, per tal de vetllar per el correcte funcionament i ús adequat del sistema.
  
### Model

Els usuaris registrats poden ser professors o alumnes.

Els professors poden crear continguts i cursos.

Els professors pengen materials (públics o privats) per als seus alumnes o per a la comunitat. Els materials públics poden ser utilitzats per altres professors i visualitzats per qualsevol alumne, mentre que els privats només els pot veure el professor creador o els usuaris participants a un curs que els referencïi. 

Els materials poden ser principalment documents de text (markdown, exportables a pdf) o formularis.

Els formularis poden ser avaluats, i és el creador qui defineix els camps (preguntes) que contenen, el tipus de resposta (text, numèric, fitxer, imatge, resposta múltiple...) i la valoració obtinguda, així com un model de correcció (opcional) per a que els alumnes que accedeixin als continguts públics puguin realitzar una autoavaluació.

- L'edició de documents de text es farà amb un editor WYSWYG, de forma que els professors editors i creadors de contingut puguin realitzar la edició sense saber markdown.
- Els formularis disposaran d'un o més camps, que poden ser de diversos tipus (text, numèric, text llarg, document, imatge, sel·lecció simple o múltiple...)
    - De moment no s'implementarà una correcció automàtica, per tal de no complicar en excés l'abast del projecte.
- Els continguts públics no poden ser eliminats de forma normal.

Un curs pot tenir continguts associats (0..n), alumnes (0..n), una data de començament i una data de finalització.
- Un curs pot tenir un o més professors associats, on un d'ells serà el creador.
  - Per a això, el creador del curs enviarà invitació a altres professors.
- En cas de que existeixi més d'un professor, les decisions rellevants sobre el curs es prendran mitjançant una votació.
- Un curs es pot arxivar en cas de que s'estableixi que no es pujaran continguts nous ni es realitzaran avaluacions, o bé hagi finalitzat.
  - Els continguts d'un curs arxivat es poden visualitzar.
  - Un curs arxivat no admet l'addició d'alumnes, professors adjunts o continguts.
- Els cursos es poden clonar.
  - En clonar un curs, es conserven els continguts, però no les avaluacions ni els professors ni els alumnes.
  

Els materials associats a un curs poden tenir un temps de visibilització, ser sempre visibles o sempre invisibles.
- La visibilització només afecta als alumnes associats.
- El professor o professors editor/s poden modificar lliurement la visibilització del material.
Al afegir material a un curs, aquest pot ser públic o privat, i serà visible per tots els alumnes matriculats independentment d'això. Es pot afegir a un curs material públic creat apart, fins i tot si ha sigut creat per altres professors.
- El material creat des de zero per a un curs s'associa a aquest i és creat com a material privat.
- Al eliminar un curs, els materials no són eliminats, però s'ofereix al propietari dels materials decidir si eliminar o no els materials privats que únicament estiguin assignats al curs.
  - Si el curs té més d'un professor associat, tots han d'estar d'acord en l'eliminació del curs, que es farà mitjançant un procés de votació i l'eliminació s'autoritzarà si tots els professors hi estan d'acord.
  - Cada professor decideix sobre el seu material privat en la votació.
  - En cap cas s'eliminarà ni donarà opció d'eliminar cap material públic associat a un curs eliminat, independentment de la seva autoria.

Dins d'un curs, es poden crear evaluacions associades a un material per a tots els seus alumnes, així com assignar una ponderació sobre la seva valoració per tal de determinar una evaluació global. 
- Al eliminar un curs, s'eliminen totes les evaluacions associades al curs. Per a evitar això, és recomanable arxivar el curs un cop finalitzat.

Un material públic d'un altre professor es pot extendre. Es conserva l'autoria al creador original i a tots els autors de les extensions anteriors de la qual es parteixi.
Extendre un material crea un "clon" del material original, sobre el qual el professor que l'extén pot treballar per a modificar-lo o ampliar-lo. 
- En cas que el material original sigui modificat o eliminat per el seu autor, les seves extensions es conserven inalterades, però la referència a l'autor seguirà existint.


