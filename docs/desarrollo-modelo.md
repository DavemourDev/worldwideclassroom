# Modelo de WorldWide ClassRoom

## Explicación

WorldWide ClassRoom es un repositorio de apuntes y de cursos para profesores y estudiantes de diversas disciplinas.


## Entidades

### Usuario

Un usuario puede actuar como profesor o como estudiante.

Se guarda información básica, como el nombre, el género, la fecha de nacimiento, el número de teléfono y el e-mail.

Al crear el perfil, se elige uno de los dos roles.

### Estudiante

Un estudiante puede visualizar contenidos públicos, al igual que un usuario no registrado.
Puede visualizar también contenidos privados de cursos en los que esté matriculado, siempre que éstos sean visibles.

Además, puede solicitar matricularse en cursos y realizar presentación de tareas de los cursos en los que esté matriculado de forma efectiva.

### Profesor

Un profesor puede visualizar contenidos, crearlos y extenderlos.
Puede visualizar incluso contenidos privados si son de su autoría.

### Contenido

Un 

## Relaciones

#### Profesor - Contenido (1-n)

Un profesor puede crear y administrar contenidos, que pueden ser públicos (cualquier estudiante o profesor puede visualizarlos) o privados (solamente el profesor creador puede accederlos, si no están asociados a ningún curso).

#### Profesor - Curso (1-n)

Un profesor puede crear varios cursos y administrarlos, pero un curso tendrá un único administrador principal.

Si el administrador principal desea abandonar el curso, se asignará a otro profesor como administrador. Esto se puede hacer mediante votación o como decisión única del administrador.

#### Profesor - Curso (m-n: EdicionCurso)

Varios profesores pueden ser editores de un curso, así como varios cursos pueden tener como uno de sus editores al mismo profesor.

Para que un profesor sea editor de un curso, el administrador debe enviarle una invitación a dicho profesor y éste debe aceptarla. 

Se crea la entidad de este tipo en el momento que se envía la solicitud, con lo cual la participación solamente se hará efectiva en el momento que sea aceptada.

Esto evita también crear varias invitaciones al mismo curso al mismo profesor, haciendo más manejable su gestión.

#### Alumno - Curso (m-n: Matrícula)

Un alumno puede estar matriculado o no en uno o más cursos. Del mismo modo que para la participación, se puede enviar una solicitud de matrícula, la cual deberá ser aceptada por el administrador del curso o por un profesor asociado al mismo si el administrador lo prefiere.

#### Curso - Contenido (m-n: IntegraciónContenido)

Un contenido puede o no formar parte de uno o de más cursos.

La interfaz permite a un profesor asociar contenidos existentes (públicos o privados con su autoría) o bien crear contenidos específicamente para el curso.

Los contenidos privados asociados pueden ser visualizados por todos los participantes efectivos del curso en tanto que pertenezcan a él, pero no extendidos por nadie que no sea el propio autor.

En integración de contenidos, se permite a los editores configurar la visualización de los contenidos, ya sea por intervalo de fecha o por apertura o cierre explícitos.

#### Contenido - Contenido (1-n: Extensión)

Un contenido puede ser creado de cero o a partir de otro contenido existente.

La extensión se permite para cualquier contenido visualizable por el profesor de forma normal (no incluyendo en este caso, por ejemplo, contenidos privados de profesores pertenecientes al curso).

Una extensión siempre es pública.

> No obstante, se estudia la posibilidad de que se puedan crear extensiones privadas en casos especiales, como por ejemplo, de cara a crear contenidos para un curso.

Los contenidos que han sido extendidos guardan entre sí sus correspondientes referencias bilateralmente.

> Cabe destacar que se plantea la casuística de poder reportar contenidos sospechosos de plagio, teniendo en cuenta las consideraciones que implica la filosofía de la aplicación como repositorio público de contenidos.

#### Curso - Tarea (1 - n)

Una tarea pertenece a un único curso. Un curso puede o no tener una o varias tareas.

#### Tarea - Contenido (1 -1)

A una tarea se asocia un contenido del curso.

La tarea tiene dependencia del contenido para existir, pero no al revés.

#### Tarea - Alumno (m - n: Presentación) 

Un alumno de un curso puede realizar una presentación de una tarea. Los datos de la presentación se basan en el mismo modelo que el contenido de la tarea.
