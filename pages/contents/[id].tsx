import { QuizContentDetail, TextContentDetail } from "@/components/contents/ContentDetail";
import Layout from "@/components/Layout";
import { Content } from "@/interfaces/index";
import { fetchAllContentsData, fetchContentById } from "@/utils/fetch-data";
import React from "react";


type ContentDetailPageProps = {
    content: Content
}
const ContentDetailPage = ({ content }: ContentDetailPageProps) => {

    console.log({content});

    switch (content.type) {
        case 'text':
            return ( 
                <Layout title={ content.title }>
                    <TextContentDetail content={content}/>
                </Layout>
            );
        case 'form':
            return ( 
                <Layout title={ content.title }>
                    <QuizContentDetail content={content}/>
                </Layout>
            ) 
    }

}


export const getStaticPaths = async () => {

    const contentsData = await fetchAllContentsData();
    const locales = ["es", "ca", "en"];
    const paths = []; 
    
    for (let locale of locales) {
        paths.push(...contentsData.map((c: Content) => ({ params: { id: c.id}, locale })));
    }

    return {
        paths,
        fallback: false
    };
} 


export const getStaticProps = async ({params}: {params: any}) => {
    const content = await fetchContentById(params.id);
    return { props: { content }, revalidate: 600 };
}

export default ContentDetailPage;