import "@/styles/global.scss";
import type { AppProps } from "next/app";

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import AuthProvider from "@/components/hooks/auth-context";
import ToastProvider from "@/components/hooks/toast-context";
import ContentProvider from "@/components/hooks/content-context";
import ModalProvider from "@/components/hooks/modal-context";
import CourseProvider from "@/components/hooks/course-context";


// TODO: Encapsular iconos
library.add(fab, fas);

const WWCRApp = ({ Component, pageProps }: AppProps) => {
  
  return (
      <ToastProvider>
        <ModalProvider>
          <AuthProvider>
            <ContentProvider>
              <CourseProvider>
                <Component {...pageProps} />
              </CourseProvider>
            </ContentProvider>
          </AuthProvider>
        </ModalProvider>
      </ToastProvider>
  );
};

export default WWCRApp;
