import { NextApiRequest, NextApiResponse } from "next";
import Users from "@/model/User";
import withMongo from "@/middleware/mongodb";

const handler = async (_request: NextApiRequest, response: NextApiResponse) => {
  try {
    const users = await Users.find();
    response.status(200).json(users);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export default withMongo(handler);
