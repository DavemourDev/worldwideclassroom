import { NextApiRequest, NextApiResponse } from "next";
import Courses from "@/model/Course";
import withMongo from "@/middleware/mongodb";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  const userId = request.query.id as string;

  try {
    const userCourses = await Courses.findByAuthorId(userId);

    console.log({ userCourses });

    response.status(200).json(userCourses);
  } catch (error) {
    console.log(error);

    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export default withMongo(handler);
