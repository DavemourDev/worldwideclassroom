import { NextApiRequest, NextApiResponse } from "next";
import Users from "@/model/User";
import withMongo from "@/middleware/mongodb";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  const id = request.query.id;
  const { description } = request.body;

  console.log({
    request: "Change user description",
    id,
    description,
  });

  try {
    await Users.findOneAndUpdate({ id }, { description }, { new: true });
    response.status(200).json({
      success: true,
      message: "Description successfully changed",
    });
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export default withMongo(handler);
