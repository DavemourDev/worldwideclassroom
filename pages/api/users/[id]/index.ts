import { NextApiRequest, NextApiResponse } from "next";
import Users from "@/model/User";
import withMongo from "@/middleware/mongodb";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  try {
    const user = await Users.findOne({
      id: request.query.id,
    });
    response.status(200).json(user);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export default withMongo(handler);
