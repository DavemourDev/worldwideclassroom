import { NextApiRequest, NextApiResponse } from "next";
import Contents from "@/model/Content";
import withMongo from "@/middleware/mongodb";
import nextConnect from "next-connect";

const handler = nextConnect();

handler.get(async (request: NextApiRequest, response: NextApiResponse) => {
  const userId = request.query.id as string;

  try {
    const userContents = await Contents.findByAuthorId(userId);

    console.log({ userContents });

    response.status(200).json(userContents);
  } catch (error) {
    console.log(error);

    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

export default withMongo(handler);
