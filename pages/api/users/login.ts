import { NextApiRequest, NextApiResponse } from "next";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import withMongo from "../../../middleware/mongodb";

import User from "@/model/User";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  const { body } = request;

  console.log({ req: "login", body });

  // TODO: Validar esquema body

  try {
    const user = await User.findOne({ username: body.username });

    if (!user) {
      throw new Error("Wrong Credentials");
    }

    const validPassword = await bcrypt.compare(body.password, user.password);

    if (!validPassword) {
      throw new Error("Wrong Credentials");
    }

    const token = jwt.sign(
      {
        username: user.username,
        id: user.id,
      },
      process.env.TOKEN_SECRET as string
    );

    response.setHeader("auth-token", token);

    response.status(200).json({
      success: true,
      message: "Logged in successfully",
      token,
    });
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export default withMongo(handler);
