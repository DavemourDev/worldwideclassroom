import UserModel from "@/model/User";
import { NextApiRequest, NextApiResponse } from "next";
import withMongo from "../../../middleware/mongodb";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  if (request.method === "POST") {
    const { username, password, fullName, picture, role } = request.body;

    const user = new UserModel({
      username,
      password,
      fullName,
      picture,
      role,
    });

    try {
      const savedUser = await user.save();

      if (savedUser) {
        response.json({
          success: true,
          message: "User registered successfully",
          savedUser,
        });
      } else {
        response.json({
          success: false,
          message: "Register failed by some reasons...: ",
        });
      }
    } catch (error) {
      response.json({
        success: false,
        message: error.message,
      });
    }
  }
};

export default withMongo(handler);
