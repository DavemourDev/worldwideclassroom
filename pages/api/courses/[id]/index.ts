import { NextApiRequest, NextApiResponse } from "next";
import Course from "@/model/Course";
import withMongo from "@/middleware/mongodb";
import nextConnect from "next-connect";

const handler = nextConnect();

handler.get(async (request: NextApiRequest, response: NextApiResponse) => {
  const courseId = request.query.id as string;

  try {
    const course = await Course.findOne({ id: courseId });

    response.status(200).json(course);
  } catch (error) {
    console.log(error);

    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

export default withMongo(handler);
