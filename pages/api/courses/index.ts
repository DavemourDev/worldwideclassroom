import { NextApiRequest, NextApiResponse } from "next";
import CourseModel from "@/model/Course";

import withMongo from "../../../middleware/mongodb";
import nextConnect from "next-connect";

const handler = nextConnect();

handler.get(async (_request: NextApiRequest, response: NextApiResponse) => {
  try {
    const courses = await CourseModel.find();
    response.status(200).json(courses);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

handler.post(async (request: NextApiRequest, response: NextApiResponse) => {
  console.log({ request: "Post course", body: request.body });

  const newCourse = new CourseModel(request.body);

  try {
    await newCourse.save();
    response.status(200).json({
      success: true,
      message: "Course created successfully",
    });
  } catch (error) {
    console.log({ error });
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

export default withMongo(handler);
