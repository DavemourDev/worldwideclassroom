import withMongo from "../../middleware/mongodb";
import { NextApiRequest, NextApiResponse } from "next";

const handler = async (_request: NextApiRequest, response: NextApiResponse) => {
  response.status(200).json({ message: "Welcome to WWCR API!" });
};

export default withMongo(handler);
