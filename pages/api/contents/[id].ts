import { NextApiRequest, NextApiResponse } from "next";
import Contents from "@/model/Content";
import nextConnect from "next-connect";

const handler = nextConnect();

// For using middleware: handler.use(middlewareFunc)

handler.get(async (request: NextApiRequest, response: NextApiResponse) => {
  const { id } = request.query;

  try {
    const content = await Contents.findOne({ id });
    response.status(200).json(content);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

export default handler;
