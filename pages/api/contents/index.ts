import { NextApiRequest, NextApiResponse } from "next";
import ContentModel from "@/model/Content";

import withMongo from "../../../middleware/mongodb";
import nextConnect from "next-connect";

const handler = nextConnect();

handler.post(async (request: NextApiRequest, response: NextApiResponse) => {
  console.log({ request: "Post content", body: request.body });

  const newContent = new ContentModel(request.body);

  try {
    await newContent.save();
    response.status(200).json({
      success: true,
      message: "Content created successfully",
    });
  } catch (error) {
    console.log({ error });
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

handler.get(async (_request: NextApiRequest, response: NextApiResponse) => {
  try {
    const contents = await ContentModel.find();
    response.status(200).json(contents);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

// TODO: Usar withMongo como middleware

export default withMongo(handler);
