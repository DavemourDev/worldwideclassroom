import { NextApiRequest, NextApiResponse } from "next";
import ContentModel from "@/model/Content";

import withMongo from "../../../middleware/mongodb";
import nextConnect from "next-connect";

const handler = nextConnect<NextApiRequest, NextApiResponse>();

handler.get(async (_request: NextApiRequest, response: NextApiResponse) => {
  console.log("Get public contents");

  try {
    const publicContents = await ContentModel.findAllPublic();

    console.log({ publicContents });

    response.status(200).json(publicContents);
  } catch (error) {
    response.status(400).json({
      success: false,
      message: error.message,
    });
  }
});

export default withMongo(handler);
