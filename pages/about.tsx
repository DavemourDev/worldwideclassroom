import useDictionary from '@/components/hooks/use-dictionary';
import ReactMarkdown from 'react-markdown';

import Layout from '../components/Layout'

const AboutPage = () => {

  const { aboutContent } = useDictionary();

  return (
    <Layout title="WWCR - About">
      <ReactMarkdown>{ aboutContent }</ReactMarkdown>
    </Layout>
)
};

export default AboutPage;
