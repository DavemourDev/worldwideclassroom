
import { useAuth } from '@/components/hooks/auth-context'
import Layout from '../components/Layout'
import { useRouter} from 'next/router';
import styles from '@/styles/Home.module.scss';
import useDictionary from '@/components/hooks/use-dictionary';
import ReactMarkdown from 'react-markdown';


const IndexPage = () => {
  const router = useRouter();

  const {loggedInUser} = useAuth();
  const { landingPageHeading, landingPageSection1, landingPageSection2, landingPageSection3 } = useDictionary();


  // Si hay un usuario logueado en el contexto de la aplicación,
  // redirigir a panel de usuario 
  if (loggedInUser != null) {
    router.push('/user', undefined, { shallow: true })
  }

  return (
    <Layout title="WWCR - Home">
      <div className={ styles.jumbotron}>
        <hgroup>
          <ReactMarkdown>{ landingPageHeading }</ReactMarkdown>
        </hgroup>
      </div>
      
      <div className={ styles.landingSection1 }>
        <div className={ styles.background }></div>
        <div className={ styles.content }>
          <ReactMarkdown>{ landingPageSection1 }</ReactMarkdown>
        </div>
      </div>

      <div className={ styles.landingSection2 }>
        <div className={ styles.background }></div>
        <div className={ styles.content }>
          <ReactMarkdown>{ landingPageSection2 }</ReactMarkdown>
        </div>
      </div>

      <div className={ styles.landingSection3 }>
        <div className={ styles.background }></div>
        <div className={ styles.content }>
          <ReactMarkdown>{ landingPageSection3 }</ReactMarkdown>
        </div>
      </div>
    </Layout>
  );
}


export default IndexPage
