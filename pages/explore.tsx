import Layout from "@/components/Layout";
import { ContentList } from "@/components/contents/ContentList";
import { Content, Course } from "@/interfaces/index";
import React from "react";
import { CourseList } from "@/components/courses/CourseList";
import { useState } from "react";
import { fetchAllCoursesData, fetchPublicContentsData } from "@/utils/fetch-data";
import SearchField from "@/components/forms/fields/SearchField";
import useDictionary from "@/components/hooks/use-dictionary";
import styles from '@/styles/Explore.module.scss';


type ContentIndexPageProps = {
    contents: Content[],
    courses: Course[]
}

const ContentIndexPage = ({ contents, courses }: ContentIndexPageProps) => {
 
  const [ filteredContents, setFilteredContents] = useState<Content[]>(contents);
  const [ filteredCourses, setFilteredCourses ] = useState<Course[]>(courses);
 
  const { contentsLabel, coursesLabel, exploreNavLabel, searchByTitleLabel: searchByTitle } = useDictionary(); 

  return (
    <Layout title="WWCR - Contents">
        <h1>{ exploreNavLabel}</h1>
        <div className={ styles.explorePanel}>
          <div className={ styles.panel }>
            <h2>{ contentsLabel }</h2>
            <SearchField label={ searchByTitle } labelIcon="search" changeHandler={ searchVal => {
              setFilteredContents(contents.filter(c => !searchVal || c.title.toLowerCase().includes(searchVal.toLowerCase())))
            }}/>
            <ContentList contents={ filteredContents} showAuthor/>
          </div>
          <div className={ styles.panel }>
            <h2>{ coursesLabel }</h2>
            <SearchField label={ searchByTitle} labelIcon="search" changeHandler={ searchVal => {
              setFilteredCourses(courses.filter(c => !searchVal || c.title.toLowerCase().includes(searchVal.toLowerCase())))
            }}/>
            <CourseList courses={ filteredCourses}/>
          </div>
        </div>
    </Layout>
  )
};


export const getServerSideProps = async () => {

  const contentsData = await fetchPublicContentsData();
  const coursesData = await fetchAllCoursesData();

  return {
    props: { 
     contents: contentsData,
     courses: coursesData
    }
  };
}

export default ContentIndexPage;