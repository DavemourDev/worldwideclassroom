import Layout from "@/components/Layout";
import React, { useEffect, useState } from "react";
import styles from '@/styles/UserPanel.module.scss'
import { ContentList } from "@/components/contents/ContentList";
import { CourseList } from "@/components/courses/CourseList";
import { useAuth } from "@/components/hooks/auth-context";
import { useRouter } from "next/router";
import { useContents } from "@/components/hooks/content-context";
import { Content, Course } from "../interfaces/index";
import { useCourses } from "@/components/hooks/course-context";
import { UserPanelHeader } from "@/components/user/UserPanelHeader";
import useDictionary from "@/components/hooks/use-dictionary";

const UserPage = () => {

    const router = useRouter();
    const { loggedInUser } = useAuth();
    const { getCoursesByAuthor, showNewCourseForm } = useCourses();
    const { getContentsByAuthor, showNewContentForm } = useContents();
 
    const [isBrowser, setIsBrowser] = useState(false);
    const [ userContents, setUserContents ] = useState<Content[]>([]);
    const [ userCourses, setUserCourses ] = useState<Course[]>([]);
    
    const { createTextLabel, createQuizLabel, myContentsLabel, myCoursesLabel, createCourseLabel} = useDictionary();

    useEffect(() => {
        setIsBrowser(true);
        if (loggedInUser) {
             getContentsByAuthor(loggedInUser.id).then(contents => setUserContents(contents || []));
             getCoursesByAuthor(loggedInUser.id).then(courses => setUserCourses(courses || []));
        } else if (isBrowser) {
            router.push('/', undefined, {shallow: true})
            return;
        }
    }, []);

    return ( loggedInUser &&
    <Layout title="WWCR - User panel">
        <div className={styles.userPanel}>
            
            <UserPanelHeader user={ loggedInUser }/>
            <div className={ styles.board }>
                <div className={ styles.contentPanel }>
                    <h2>{ myContentsLabel }</h2>
                    <ContentList contents={ userContents}/>
                    { ( loggedInUser.role == 'teacher') ? (
                        <div className={styles.buttonGroup}>
                            <button onClick={ () => showNewContentForm("text", loggedInUser.id) }>
                                { createTextLabel}
                            </button>
                            <button onClick={ () => showNewContentForm("form", loggedInUser.id) }>
                                { createQuizLabel}
                            </button>
                        </div>
                    ): null }
                
                </div>
                <div className={ styles.coursePanel }>
                    <h2>{ myCoursesLabel}</h2>
                    <CourseList courses={ userCourses }/>
                   
                    <div className={styles.buttonGroup}>
                        { (loggedInUser.role == 'teacher') ? (
                            <button onClick={ () => showNewCourseForm(loggedInUser.id) }>
                                { createCourseLabel}
                            </button>
                        ) : null }
                    </div>
                </div>
            </div>
        </div>
    </Layout>
)};

export default UserPage;