const path = require("path");

const removeImports = require("next-remove-imports")({
  options: {},
  matchImports: "\\.(less|css|scss|sass|styl)$",
});

module.exports = removeImports({
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  env: {
    HOST: "http://localhost:3000",
    TOKEN_SECRET:
      "nbxc74098n709bcv78t29304n578c9gn5yh90c5g87905678h93nhc567n8h09hbn576h9856huc560y9ib9m34y90hnuj6y09b",
    MONGO_URI:
      "mongodb+srv://wwcr-local:QZvepVVZ7I6vbwUb@cluster0.u0wma.mongodb.net/wwcr?retryWrites=true&w=majority",
  },
  i18n: {
    locales: ["en", "es", "ca"],
    defaultLocale: "en",
  },
  async redirects() {
    return [];
  },
});
