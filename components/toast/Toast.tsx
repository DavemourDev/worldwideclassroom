import React, { useEffect, useState } from "react"
import ReactDOM from "react-dom";
import styles from '@/styles/Toast.module.scss';

export type ToastType = 'alert' | 'success' | 'warning' | 'info' | 'danger';

type ToastProps = {
    message: string,
    type: ToastType
    onClose: () => void
    opened: boolean
    isClosing: boolean
}

export const Toast = ({ message, type, onClose, opened = false, isClosing=false }: ToastProps) => {


    const [ isBrowser, setIsBrowser ] = useState(false);
    const [ closed, setClosed] = useState(!opened);

    useEffect(() => {
        setIsBrowser(true);
        setClosed(true);
    }, []);

    useEffect(() => {
        if (closed) {
            onClose();
        }
    }, [closed]);

    return (
        isBrowser && opened ? ReactDOM.createPortal(
            
            <div className={ styles.toast + ' ' + styles[type] + ' ' + ( isClosing && styles.hidden ) }>
               { message }
            </div>
        , document.getElementById("modal-root") as Element) : null
    )
};