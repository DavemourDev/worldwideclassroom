import { IconProp } from "@fortawesome/fontawesome-svg-core";
import styles from "@/styles/Form.module.scss"; 
import React from "react";
import { Label } from "./Label";

// TODO: Leer de módulo de tipos
type Extension = ".jpg" | ".jpeg" | ".png" | ".gif" | ".mov";

type FileFieldProps = {
    label: string;
    labelIcon: IconProp;
    changeHandler: (value: File | null) => void;
    extensions: Extension[];
}

const FileField = ({ label, labelIcon, changeHandler}: FileFieldProps) => {

    return (
        <div className={ styles.formField }>
            <Label icon={ labelIcon} text={ label} />  
             <input type="file" accept="image/png, image/jpeg" className={ styles.input } onChange={ (ev) => {
                const files = ev.target.files;
                if (files != null) {
                    changeHandler(files[0])
                }
            }}/>
        </div>
    );

}

export default FileField;