import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import styles from "@/styles/Form.module.scss"; 

type SearchFieldProps = {
    label: string;
    labelIcon: IconProp;
    changeHandler: (value: string) => void;
}


const SearchField = ({ label, labelIcon, changeHandler }: SearchFieldProps) => {
    return (
    <div className={ styles.formField }>
        <label className={ styles.label}><FontAwesomeIcon scale={ 1.25 } icon={labelIcon }/>&nbsp;{ label }</label>
        <input className={ styles.input} type="search" onChange={ (ev) => changeHandler(ev.target.value)}/>
    </div>)
}

export default SearchField;