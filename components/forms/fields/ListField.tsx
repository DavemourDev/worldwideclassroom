import { IconProp } from "@fortawesome/fontawesome-svg-core";
import styles from "@/styles/Form.module.scss"; 
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

type ListFieldProps = {
    label: string,
    labelIcon: IconProp
    changeHandler: (value: string[]) => void,
    options: string[]
}

const ListField = ({ label, labelIcon, changeHandler, options }: ListFieldProps) => {

    const [selectedValues, setSelectedValues ] = useState<number[]>([]);

    return (
        <div className={ styles.formField + ' ' + styles.listSelector}>
            <label className={ styles.label}><FontAwesomeIcon scale={ 1.25 } icon={labelIcon}/>&nbsp;{ label }</label>
            
                { 
                    options.map((opt: string, index: number) => {
                        
                        const id = "option_" + label + index;
                        
                        return (
                        <div key={ id } className={styles.option + ' ' + (selectedValues.includes(index) ? styles.selected : "")}>
                            <input id={ id } name={ label } type="checkbox" value={opt} onChange={
                                () => {
                                    if (selectedValues.includes(index)) {
                                        setSelectedValues(selectedValues.filter(v => v !== index));
                                    } else {
                                        setSelectedValues([...selectedValues, index]);
                                    }

                                    changeHandler(options.filter((_opt, index: number) => selectedValues.includes(index)))
                                }
                            }
                            />
                            <label htmlFor={ id }>
                                { opt }</label>
                        </div>
                    )})
                }
            
        </div>


    );


}



export default ListField;