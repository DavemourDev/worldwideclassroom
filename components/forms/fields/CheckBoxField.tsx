import styles from "@/styles/Form.module.scss"; 

import { useEffect, useState } from "react"

type CheckBoxFieldProps = {
    label?: string;
    initialValue?: boolean;
    changeHandler: (value: boolean) => void;
    allowUncheck?: boolean
}

const CheckBoxField = ({ label = "", initialValue=false, changeHandler, allowUncheck=false}: CheckBoxFieldProps) => {

    const [checked, setChecked] = useState<boolean>(initialValue)

    useEffect(() => {
        changeHandler(checked)
    }, [checked]);

    return (
        <div className={ styles.checkBox+ " " + styles.checkBox}>
            <input type="checkbox" checked={ checked } onClick={ _ev => { 
                if (!allowUncheck && checked) {
                    return;
                }
            
                setChecked(!checked)} 
            }/>
            { label && <label>{ label }</label>}
        </div>
    )

} 

export default CheckBoxField;