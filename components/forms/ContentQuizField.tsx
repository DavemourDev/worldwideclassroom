import { Question } from "@/interfaces/index";
import styles from "@/styles/Form.module.scss";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Label } from "./Label";
import TextField from "./TextField";

const MAX_QUESTIONS = 5;

type ContentQuizFieldProps = {
    labelIcon : IconProp,
    label: string,
    defaultValue?: Question[],
    changeHandler: (value: Question[]) => void
};

type QuestionFieldProps = {
    index: number;
    question?: Question,
    changeHandler: (question: Question) => void
}

const QuestionField = ({index, changeHandler}: QuestionFieldProps) => {

    const [ question, setQuestion ] = useState("");
    const [ answers, setAnswers ] = useState(["", "", "", ""]);
    const [ correct, setCorrect ] = useState(0);

    useEffect(() => {
        changeHandler({
            question, answers, correct
        });
    }, [question, answers, correct])

    return (
        <div className={ styles.formField }>
            <h4 className={styles.question}> 
                <TextField label={ `Question ${index+1}`} labelIcon="question" changeHandler={ setQuestion }/>
            </h4>
            <ul className={ styles.answers} >
                { answers.map((answer: string, index) => (
                    <li key={ index}>
                        <input type="checkbox" className={ styles.checkbox} checked={ index === correct} onChange={ _ev => setCorrect(index) }/>
                        <TextField label={`Answer ${index + 1}`} labelIcon="dice" initialValue={ answer }  changeHandler={ (value) => {
                            answers[index] = value;
                            setAnswers([...answers])
                        } }/>
                    </li>
                )) }
            </ul>
        </div>
    )
};


export const ContentQuizField = ({ labelIcon, label, defaultValue=[
    {
        question: "",
        answers: ["", "", "", ""],
        correct: 0
    }
], changeHandler }: ContentQuizFieldProps) => {

    const [questions, setQuestions] = useState<Question[]>(defaultValue);
    const [ currentIndex, setCurrentIndex] = useState<number>(0);

    useEffect(() => {
        changeHandler(questions);
    }, [questions])

    return (

        <div className={styles.formField}>
            <Label icon={ labelIcon} text={ label } />
            <div className="button-group">
                <button 
                    type="button" 
                    onClick={ () =>  setCurrentIndex(currentIndex - 1) } 
                    disabled={ currentIndex <= 0}
                ><FontAwesomeIcon icon="arrow-left"/></button>
                {
                    questions.map((_q, index) => (
                        <button type="button" className={ currentIndex === index ? styles.active : "" } onClick={() => setCurrentIndex(index)}>
                            Question { index + 1}
                        </button>
                    ))
                }
                <button 
                    type="button" 
                    onClick={ () =>  setCurrentIndex(currentIndex + 1) } 
                    disabled={ currentIndex >= questions.length - 1}
                ><FontAwesomeIcon icon="arrow-right"/></button>
                

                <button type="button" onClick={ () => {
                    // TODO: Implementar interfaz de campos
                    const v: Question = {
                        question: "",
                        answers: ["", "", "", ""],
                        correct: 0
                    }
                    questions.push(v);

                    setQuestions([...questions]);
                }} disabled={ questions.length >= MAX_QUESTIONS }><FontAwesomeIcon icon="plus"/></button>


            </div>
            <QuestionField  
                index={ currentIndex } 
                question={ questions[currentIndex] }
                changeHandler = {(question) => {
                    questions[currentIndex] = question;
                    setQuestions([...questions]);
                }}
            />
        </div>
    )
}