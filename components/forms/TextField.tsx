import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import styles from "@/styles/Form.module.scss"; 
import { useEffect, useState } from "react";

type TextFieldProps = {
    label: string;
    labelIcon: IconProp;
    changeHandler: (value: string) => void;
    secret?: boolean;
    long? : boolean;
    initialValue?: string;

}


const TextField = ({ label, labelIcon, changeHandler, secret=false, long=false, initialValue="" }: TextFieldProps) => {
    
    const [value, setValue] = useState(initialValue);
    
    useEffect(() => {
        changeHandler(value);
    }, [value]);

    return (
    <div className={ styles.formField }>
        <label className={ styles.label}><FontAwesomeIcon scale={ 1.25 } icon={labelIcon }/>&nbsp;{ label }</label>
        {
            long ? 
            (
                <textarea className={ styles.textarea } onChange={ ev => setValue(ev.target.value) }>{ initialValue }</textarea>
            )
            : <input className={ styles.input} type={secret? "password" : "text"}  value={ value } onChange={ (ev) => setValue(ev.target.value)}/>    

        }
    </div>)
}

export default TextField;