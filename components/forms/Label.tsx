
import styles from "@/styles/Form.module.scss";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

type LabelOptions = {
    scale?: number,
    for?: string
};

type LabelProps = {
    text: string,
    icon: IconProp,
    options?: LabelOptions
};

export const Label = ({text, icon, options={}}: LabelProps) => (
    <label htmlFor={options.for || undefined } className={styles.label}>
        <FontAwesomeIcon scale={ options.scale || 1.25} icon={ icon } />
        &nbsp;{text}
    </label>
);
