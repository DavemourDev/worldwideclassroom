import { useState } from "react";
import TextField from '@/components/forms/TextField';

type DescriptionFormProps = {
    onSubmit: (description: string) => void;
    sendButtonLabel: string;
    title: string;
    initialDescription?: string;
}



export const DescriptionForm = ({ onSubmit, title, sendButtonLabel, initialDescription = "" }: DescriptionFormProps) => {
 
    const [ description, setDescription ] = useState<string>(initialDescription);

    return (
    <form onSubmit={ ev => {
            ev.preventDefault();
            onSubmit(description);
        }}>
            <h2>{ title }</h2>
            <TextField label="description" labelIcon="comment" changeHandler={ setDescription } long/>
            <button type="submit">{ sendButtonLabel }</button>
        </form>
    );
}

