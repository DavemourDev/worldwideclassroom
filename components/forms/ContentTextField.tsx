import MDEditor, { commands } from "@uiw/react-md-editor";
import "@uiw/react-md-editor/dist/markdown-editor.css";
import "@uiw/react-markdown-preview/dist/markdown.css";
import styles from '@/styles/Form.module.scss';
import React, { useEffect, useState } from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Label } from "./Label";

type ContentTextFieldProps = {
    label: string,
    labelIcon: IconProp,
    changeHandler: (content: string) => void
    value?: string
}

/*Ejemplo comando personalizado:
const title3: ICommand = {
    name: 'title3',
    keyCommand: 'title3',
    buttonProps: { 'aria-label': 'Insert title3'},
    icon: (
        <svg width="12" height="12" viewBox="0 0 520 520">
            <path fill="currentColor" d="M15.7083333,468 C7.03242448,468 0,462.030833 0,454.666667 L0,421.333333 C0,413.969167 7.03242448,408 15.7083333,408 L361.291667,408 C369.967576,408 377,413.969167 377,421.333333 L377,454.666667 C377,462.030833 369.967576,468 361.291667,468 L15.7083333,468 Z M21.6666667,366 C9.69989583,366 0,359.831861 0,352.222222 L0,317.777778 C0,310.168139 9.69989583,304 21.6666667,304 L498.333333,304 C510.300104,304 520,310.168139 520,317.777778 L520,352.222222 C520,359.831861 510.300104,366 498.333333,366 L21.6666667,366 Z M136.835938,64 L136.835937,126 L107.25,126 L107.25,251 L40.75,251 L40.75,126 L-5.68434189e-14,126 L-5.68434189e-14,64 L136.835938,64 Z M212,64 L212,251 L161.648438,251 L161.648438,64 L212,64 Z M378,64 L378,126 L343.25,126 L343.25,251 L281.75,251 L281.75,126 L238,126 L238,64 L378,64 Z M449.047619,189.550781 L520,189.550781 L520,251 L405,251 L405,64 L449.047619,64 L449.047619,189.550781 Z" />
        </svg>
    ),
    execute: (state: TextState, api: TextAreaTextApi) => {
        let modifyText = `### ${state.selectedText}\n`;
        if (!state.selectedText) {
            modifyText = `### `
        }
        api.replaceSelection(modifyText);
    }
}
*/

export const ContentTextField = ({ changeHandler, value, label, labelIcon}: ContentTextFieldProps) => {

    const [ content, setContent ] = useState(value || "");

    // Configuración
    const tabSize = 1;

    useEffect(() => {
        changeHandler(content);
    }, [content])

    return (

        <div className={styles.formField}>
            <Label icon={ labelIcon} text={ label } />
            <MDEditor 
                value={ content } 
                tabSize={ tabSize }
                onChange={ (contentChanged) => {
                    setContent(contentChanged as string);
                }}
                previewOptions={{
                    skipHtml: true,
                }}
                commands={
                    [
                        commands.group([
                            commands.title1, commands.title2, commands.title3
                        ], {
                            name: 'title',
                            groupName: 'title',
                            icon: <FontAwesomeIcon icon="heading"/>,
                            buttonProps: {'aria-label': 'Insert title', 'title': 'Insert heading'},
                        }),
                        commands.group([
                            commands.bold, commands.italic, commands.code, commands.strikethrough
                        ], {
                            name: 'text-format',
                            groupName: 'text-format',
                            icon: <FontAwesomeIcon icon="font"/>,
                            buttonProps: {'aria-label': 'Text format', 'title': 'Text format'},
                        }),
                        commands.group([
                            commands.quote, commands.link, commands.unorderedListCommand, commands.hr
                        ], {
                            name: 'Document elements',
                            groupName: 'Document elements',
                            icon: <FontAwesomeIcon icon="box"/>,
                            buttonProps: {'aria-label': 'Elements', 'title': 'Document Elements'},
                        }),
                        ]}

                
                
                />

            { false && <MDEditor.Markdown source={ content} /> }
        </div>
    )

}