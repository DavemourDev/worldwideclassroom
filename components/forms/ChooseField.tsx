import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useState } from "react";
import styles from '@/styles/Form.module.scss';

type ChoiceProps = {
    label: string,
    value: any
};

type ChooseFieldProps = {
    label: String;
    labelIcon: IconProp;
    changeHandler: (value: any) => void;
    choices: ChoiceProps[]
    multiple?: boolean
}

const ChooseField = ({label, labelIcon, changeHandler, choices, multiple=false}: ChooseFieldProps) => {

    const [selected, setSelected] = useState(0);

    return (
        <div className={styles.formField + ' ' + styles.inline}>
            <label><FontAwesomeIcon icon={labelIcon}/>&nbsp;{label}</label>
            { choices.map((choice, index) => { 
                const id = "choice_" + label + "_" + choice.value;
                
                return ( 
                    <div key={ id } className={styles.option + " " + ((selected == index) ? styles.selected : "") }>
                        <input type={ multiple ? "checkbox" : "radio"} id={ id } name={ "choice_" + label } value={ choice.value } checked={ index === selected } onChange={
                            () => {
                                setSelected(index);
                                changeHandler(choices[index]);
                            }
                        }/>
                        <label htmlFor={ id }>{ choice.label }</label>
                    </div>
                )
            }) }
        </div>
    )
};

export default ChooseField;