import {
  DICTIONARY_CA,
  DICTIONARY_EN,
  DICTIONARY_ES,
} from "@/utils/dictionary";
import { useRouter } from "next/router";

const useDictionary = () => {
  const { locale } = useRouter();

  switch (locale) {
    case "es":
      return DICTIONARY_ES;
    case "ca":
      return DICTIONARY_CA;
    case "en":
    default:
      return DICTIONARY_EN;
  }
};

export default useDictionary;
