import { Course } from "@/interfaces/index";
import { fetchAllCoursesData, fetchCoursesByAuthor } from "@/utils/fetch-data";
import axios from "axios";
import React, { createContext, FC, useContext } from "react";
import { CourseForm } from "../courses/CourseForm";
import { useModal } from "./modal-context";
import { useToast } from "./toast-context";

const API_COURSES_ROUTE = `/api/courses/`;

const REQUEST_CONFIG = {
  headers: {
    "Content-Type": "application/json",
  },
};

export type CourseContextType = {
  getAllCourses: () => Promise<Course[]>;
  saveCourse: (course: Course) => Promise<boolean>;
  getCourseById: (courseID: string) => Promise<Course>;
  getCoursesByAuthor: (authorID: string) => Promise<Course[]>;
  showNewCourseForm: (authorId: string) => void;
  addContentToCourse: (courseId: string, contentId: string) => void
  // getCourseParticipants: (authorId: string) => Promise<User[]>;
};

const CourseContext = createContext<CourseContextType | null>(null);

const CourseProvider: FC = ({ children }) => {
  const { showToast } = useToast();
  const { showModal } = useModal();

  /**
   *
   * @returns
   */
  const getAllCourses = async () => {
    const courses = await fetchAllCoursesData();
    return courses;
  };

  /**
   * 
   * @param courseId 
   * @param contentId 
   * @returns 
   */
  const addContentToCourse = async (courseId: string, contentId: string) => {
    try {
      const response = await axios.post(`${API_COURSES_ROUTE}/${courseId}/contents`, {
        contentId
      }, REQUEST_CONFIG);
      return response.data;
    } catch (error) {
      showToast("danger", error.message);
    }
  }

  /**
   * Get contents created by a given author.
   * Author must be a teacher. If given author is a student, it won't return any content.
   *
   * @param authorID
   * @returns
   */
  const getCoursesByAuthor = async (authorID: string) => {
    const courses = await fetchCoursesByAuthor(authorID);
    return courses;
  };

  /**
   *
   * @param id
   * @returns
   */
  const getCourseById = async (id: string) => {
    try {
      const response = await axios.get(
        `${API_COURSES_ROUTE}/${id}`,
        REQUEST_CONFIG
      );
      return response.data;
    } catch (error) {
      showToast("danger", error.message);
    }
  };

  /**
   * Saves a given content data.
   * Can be used for creating new data or for modify existent.
   *
   * @param course
   */
  const saveCourse = async (course: Course) => {
    try {
      await axios.post(API_COURSES_ROUTE, course, REQUEST_CONFIG);
      showToast("success", "Course saved successfully");
      return true;
    } catch (error) {
      return false;
    }
  };

  /**
   * 
   * @param authorId 
   */
  const showNewCourseForm = (authorId: string) => {
    console.log({authorId})
    
    showModal(
      <CourseForm submitHandler={(course) => {
        saveCourse(course).then(() => {
          showModal(null);
        })
      }} 
        author={ authorId}
      />
      );
    };

  return (
    <CourseContext.Provider
      value={{
        getAllCourses,
        showNewCourseForm,
        saveCourse,
        getCourseById,
        getCoursesByAuthor,
        addContentToCourse
      }}
    >
      {children}
    </CourseContext.Provider>
  );
};

export default CourseProvider;

export function useCourses() {
  return useContext(CourseContext) as CourseContextType;
}
