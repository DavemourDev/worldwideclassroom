import { Content, ContentType } from "@/interfaces/index";
import axios from "axios";
import React, { createContext, FC, useContext } from "react"
import { ContentForm } from "../contents/ContentForm";
import { useModal } from "./modal-context";
import { useToast } from "./toast-context";

const API_AUTHOR_CONTENTS_ROUTE = (id: string) => `/api/users/${id}/contents`;
const API_COURSE_CONTENTS_ROUTE = (id: string) => `/api/courses/${id}/contents`;
const API_CONTENTS_ROUTE = "/api/contents";
const API_PUBLIC_CONTENTS_ROUTE = "/api/contents/public";

const REQUEST_CONFIG = {
    headers: {
        'Content-Type': 'application/json'
    }
};

export type ContentContextType = {
    getContentsByAuthor: (userID: string) => Promise<Content[]>,
    getPublicContents: () => Promise<Content[]>,
    getCourseContents: (courseID: string) => Promise<Content[]>,
    saveContent: (content: Content) => Promise<boolean>,
    getContentById: (contentID: string) => Promise<Content>;
    showNewContentForm: (type: ContentType, authorId: string) => void
}

const ContentContext = createContext<ContentContextType | null>(null);

const ContentProvider: FC = ({ children }) => {

    const { showToast } = useToast();
    const { showModal } = useModal();

    /**
     * 
     * @returns 
     */
    const getPublicContents = async () => {

        try {
            const response = await axios.get(API_PUBLIC_CONTENTS_ROUTE , REQUEST_CONFIG);
            return response.data;
        } catch (error) {
            showToast('danger', error.message);
        }
    }

    /**
     * Gets all contents from a given course
     * @param courseID 
     * @returns 
     */
    const getCourseContents = async (courseID: string) => {
        try {
            const response = await axios.get(API_COURSE_CONTENTS_ROUTE(courseID), REQUEST_CONFIG);
            return response.data;
        } catch (error) {
            showToast('danger', error.message)
        }
    }

    /**
     * Get contents created by a given author.
     * Author must be a teacher. If given author is a student, it won't return any content.
     * 
     * @param authorID 
     * @returns 
     */
    const getContentsByAuthor = async (authorID: string) => {
        try {
            const response = await axios.get(API_AUTHOR_CONTENTS_ROUTE(authorID), REQUEST_CONFIG);
            return response.data;
        } catch (error) {
            console.log({error})
            showToast('danger', error.message)
        }
    };

    /**
     * 
     * @param id 
     * @returns 
     */
    const getContentById = async (id: string) => {
        try {
            const response = await axios.get(`${API_CONTENTS_ROUTE}/${id}`, REQUEST_CONFIG);
            return response.data;
        } catch (error) {
            showToast('danger', error.message)
        }
    };

    /**
     * Saves a given content data.
     * Can be used for creating new data or for modify existent.
     * 
     * @param content 
     */
    const saveContent = async (content: Content) => {
        try {
            await axios.post(API_CONTENTS_ROUTE, content, REQUEST_CONFIG);
            showToast('success', 'Content saved successfully');   
            return true;         
        } catch (error) {
            showToast('danger', error.message)
            return false;
        }
    };

    /**
     * 
     * @param type 
     * @param userId 
     */
    const showNewContentForm = (type: ContentType, userId: string) => {
        showModal(
            <ContentForm submitHandler={ (contentData) => {
                contentData.author = userId;
                
                saveContent(contentData).then((success) => {
                    if (success) {
                        showToast("success", "Content successfully saved");
                        showModal(null);
                    } else {
                        showToast("danger", "Error trying to save content");
                    }
                } ).catch((error: Error) => {
                    showToast("danger", error.message);
                });


            }} 
                type={ type} 
            />
        );
    }

    return (
        <ContentContext.Provider value={{ getPublicContents, getCourseContents, saveContent, getContentsByAuthor, getContentById, showNewContentForm }}>
            {children}
        </ContentContext.Provider>
    );


};

export default ContentProvider;

/**
 * 
 * @returns 
 */
export function useContents() {
  return useContext(ContentContext) as ContentContextType;
}