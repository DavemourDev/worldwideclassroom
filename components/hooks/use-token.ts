import { useState } from "react";
import jwt from "jsonwebtoken";

const __storage: any = {};

const TOKEN_KEY = "token";
const STORAGE = {
  // TODO: Usar con session storage (por algún motivo no está definido)
  setItem: (key: string, value: string) => {
    __storage[key] = value;
  },
  getItem: (key: string) => {
    return __storage[key] || "{}";
  },
  removeItem: (key: string) => {
    delete __storage[key];
  },
};

const useToken = () => {
  const getToken = () => {
    const tokenString = STORAGE.getItem(TOKEN_KEY);
    const userToken = JSON.parse(tokenString as string);
    return userToken?.token;
  };

  const [token, setToken] = useState(getToken());

  const saveToken = async (userToken: any) => {
    if (userToken != null) {
      STORAGE.setItem(TOKEN_KEY, JSON.stringify(userToken));
      setToken(userToken.token);
    }
  };

  const removeToken = () => {
    STORAGE.removeItem(TOKEN_KEY);
    setToken(null);
  };

  const getTokenUser = (token: string) => {
    const user = jwt.decode(token);

    return user;
  };

  return {
    setToken: saveToken,
    removeToken,
    token,
    getTokenUser,
  };
};

export default useToken;
