import React, { createContext, useContext, useState, FC, useEffect} from 'react'
import axios from 'axios';
import { User, RoleType } from '../../interfaces';
import { useToast } from './toast-context';
import useToken from './use-token';
import { LoginForm } from '../user/LoginForm';
import { RegisterForm } from '../user/RegisterForm';
import { useModal } from './modal-context';
import {DescriptionForm} from '@/components/forms/DescriptionForm';
import { fetchUserById } from '@/utils/fetch-data';

const API_LOGIN_ROUTE = "/api/users/login";
const API_REGISTER_ROUTE = "/api/users/register";
const REQUEST_CONFIG = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
};

export type AuthContextType = {
    loggedInUser: any,
    login: (username: string, password: string) => Promise<void>,
    logout: () => Promise<void>,
    register: (username: string, password: string, fullName: string, picture: string, role: RoleType) => Promise<void>,
    showLoginForm: (onSuccess: () => void) => void,
    showRegisterForm: (onSuccess: () => void) => void,
    showDescriptionForm: (userId: string, defaultDescription: string, onSuccess: () => void) => void
};

const AuthContext = createContext<AuthContextType | null>(null);

const AuthProvider: FC = ({children}) => {
    const { showModal } = useModal();
    const { setToken, getTokenUser } = useToken();

    const [authToken, setAuthToken] = useState<string|null>(null);
    const [ loggedInUser, setLoggedInUser] = useState<User | null>(null);
    const { showToast } = useToast();

    useEffect(() =>   {
        if (authToken == null) {
            setLoggedInUser(null);
            return;
        }
        const { id } = getTokenUser(authToken) as User;

        fetchUserById(id as string).then((user) => {
            if (user) {
                setLoggedInUser(user)
            }
            showToast('success', 'Logged in successfully');

        });

    }, [authToken])

    const login = async (username: string, password: string) => {
        const response = await axios.post(API_LOGIN_ROUTE, {
            username, password
        }, REQUEST_CONFIG);

        const userToken = response.data.token;
        setAuthToken(userToken)
        localStorage.setItem("token", userToken)
    }


    const logout = async () => {
        setToken(null);
    }

    const register = async (username: string, password: string, fullName: string, picture: string, role: RoleType) => {
        const response = await axios.post<{ success: boolean, message: string}>(API_REGISTER_ROUTE, {
            username, password, fullName, picture, role
        }, REQUEST_CONFIG);
        const responseData = response.data;
        showToast(responseData.success ? 'success' : 'danger', responseData.message);
    };

    const showLoginForm = (onSuccess: () => void) => {
            showModal(
                <LoginForm onSubmit={ (username, password) => {
                        login(username, password).then(() => {
                            showModal(null);
                            onSuccess();
                        }).catch(error => {
                            showToast("danger", error.message)
                        });
                    } 
                }/>
            );  
    }

    const showRegisterForm = (onSuccess: () => void) => { 
        showModal(
            <RegisterForm onSubmit={ (username, password, fullName, picture, role) => {
                    register(username, password, fullName, picture, role).then(() => {
                        // On successful register
                        showModal(null);
                        onSuccess();
                    }).catch((error) => {
                        showToast("danger", error.message);
                    });
                }} 
            />
        )
    };

    const setUserDescription = async (userId: string, description: string) => {
        const API_ROUTE = `/api/users/${userId}/description`;
        const response = await axios.post<{ success: boolean, message: string}>(API_ROUTE, {
            id: userId,
            description
        }, REQUEST_CONFIG);
        const responseData = response.data;
        refreshUserData();
        showToast(responseData.success ? 'success' : 'danger', responseData.message);
    }

    const showDescriptionForm = (userId: string, defaultDescription: string = "", onSuccess: () => void) => {
        
        showModal(
            <DescriptionForm 
                title="Description"
                sendButtonLabel="Change description"
                initialDescription={ defaultDescription }
                onSubmit={ (description: string) => {
                    setUserDescription(userId, description).then(() => {
                        onSuccess();
                    });
                }} 
            />
        )
    }

    const refreshUserData = () => {
        if (loggedInUser) {
            fetchUserById(loggedInUser.id as string).then((user) => {
                if (user) {
                    setLoggedInUser(user)
                }
            });
        }
    }

    return (
        <AuthContext.Provider value={{ loggedInUser, login, logout, register, showLoginForm, showRegisterForm, showDescriptionForm }} >
            {children}
        </AuthContext.Provider>
    );
}

export default AuthProvider;

export function useAuth() {
  return useContext(AuthContext) as AuthContextType;
}