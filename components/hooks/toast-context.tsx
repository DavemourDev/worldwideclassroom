// TODO: Valorar
import { Toast, ToastType } from "@/components/toast/Toast";
import {
  createContext,
  FC,
  useContext,
  useEffect,
  useState,
} from "react";

const DEFAULT_TOAST_TYPE: ToastType = "info";
const TOAST_DURATION_MS = 3000;
const TOAST_BANISH_DURATION_MS = 2000;

type ToastContextType = {
  showToast: (type: ToastType, message: string, options?: any) => void;
};

type ToastOptions = {
  durationMS?: number;
  banishDurationMS?: number;
};

export const ToastContext = createContext<ToastContextType | null>(null);

const ToastProvider: FC = ({children}) => {
  const [ type, setType ] = useState<ToastType>(DEFAULT_TOAST_TYPE);
  const [ message, setMessage ] = useState<string>("");
  const [ isClosing, setIsClosing ] = useState<boolean>(false);
  const [ isOpened, setOpened ] = useState<boolean>(false);

  useEffect(() => {
    if (isClosing) {
      setTimeout(() => {
        setOpened(false);
      }, TOAST_BANISH_DURATION_MS);
    }
  }, [isClosing]);

  const showToast = (type: ToastType, message: string, options: ToastOptions = {}) => {
    setType(type);
    setMessage(message);
    setOpened(true);
    setIsClosing(false);
    onOpenToast();
    // TODO: Apply options
    console.log({toastOptions: options})
  };

  const onOpenToast = () => {
    setTimeout(() => {
      setIsClosing(true);
    }, TOAST_DURATION_MS);
  };

  return (
    <ToastContext.Provider value={{ showToast }}>
      <>
        { children }
        <Toast
          isClosing={ isClosing }
          message={ message }
          type={ type}
          opened={ isOpened }
          onClose={() => {
            // ON CLOSE TOAST...
          }}
        />
      </>
    </ToastContext.Provider>

  );
};

export function useToast() {
  return useContext(ToastContext) as ToastContextType;
}

export default ToastProvider;
