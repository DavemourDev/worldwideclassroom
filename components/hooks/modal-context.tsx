import { createContext, FC, useContext, useState } from "react"
import { ModalPanel } from "../modal/ModalPanel";
import { ReactNode } from 'react';


export type ModalContextType = {
    showModal: (content: ReactNode) => void;
}

const ModalContext = createContext<ModalContextType | null>(null);

const ModalProvider: FC = ({ children }) => {

    const [ content, setContent ] = useState<ReactNode | null>(null)

    const showModal = (modalContent: ReactNode) => {
        setContent(modalContent);
    }

    return (
        <ModalContext.Provider value={ { showModal } }>
            <>
                {children}
                <ModalPanel opened={ content != null } onClose={ () => setContent(null) }>
                    { content }
                </ModalPanel>
            </>    
        </ModalContext.Provider>
    );
};

export default ModalProvider;

export const useModal = () => {
  return useContext(ModalContext) as ModalContextType;
}