import React, { ReactNode } from 'react'
import Head from 'next/head'
import { NavigationBar } from './layout-elements/Navigation'
import { Footer } from './layout-elements/Footer'

import styles from '@/styles/Layout.module.scss'
import Link from 'next/link'

type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'This is the default title' }: Props) => {
  
  return (
    <div className={ styles.layout }>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div id="modal-root"></div>
      <header>
        <h1>
          <Link href="/">WorldWide ClassRoom</Link>
        </h1>
        <NavigationBar/>
      </header>
      <main>
        {children}
      </main>
      <Footer/>
      <div id="toast-root"></div>
    </div>
)}

export default Layout
