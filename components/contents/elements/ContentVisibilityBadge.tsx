import styles from '@/styles/Content.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

export default ({ isPublic }: {isPublic: boolean}) => {
 
    return (
        <span 
            className={ styles.visibilityBadge + ' ' + isPublic ? styles.public : styles.private
        }>{ isPublic ? (
            <FontAwesomeIcon className={ styles.public} icon="lock-open"/>
        ):(
            <FontAwesomeIcon className={styles.private} icon="lock"/>
        ) }</span>
    )

}