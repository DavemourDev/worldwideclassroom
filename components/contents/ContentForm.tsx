import React, { useState } from "react";
import { ContentTextField } from "../forms/ContentTextField";
import { ContentQuizField } from '../forms/ContentQuizField';
import TextField from "../forms/TextField";
import ChooseField from "../forms/ChooseField";
import useDictionary from "../hooks/use-dictionary";
import { Content, Quiz } from "@/interfaces/index";


// TODO: Submit handler tiene que recibir como parámetro una instancia de Content
type ContentFormProps = {
    submitHandler: (content: Content) => void,
    type: 'text' | 'form',
    creationDetails?: {
        authorID: string
    }
}

export const ContentForm = ({ submitHandler, type}: ContentFormProps ) => {
    const [ title, setTitle ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ content, setContent ] = useState<Quiz | string>([]);
    const [ visibility, setVisibility] = useState('public');

    const { titleLabel, descriptionLabel, visibilityLabel, saveContentLabel, publicLabel, privateLabel, createQuizLabel, createTextLabel } = useDictionary();

    return (
        <>
            <h2>{ type == "text" ?  createTextLabel : createQuizLabel}</h2>
            <form onSubmit={ (ev) => {
                ev.preventDefault();
                
                const isPublic = visibility == 'public'; 
                
                submitHandler({
                    title, description, type, isPublic,  content
                });
            } }>
                <TextField label={ titleLabel } labelIcon="heading" changeHandler={ setTitle }/>
                <TextField label={ descriptionLabel } labelIcon="comment" changeHandler={ setDescription } long/>
                <ChooseField label={ visibilityLabel } labelIcon="eye" changeHandler={ setVisibility } 
                    choices={ [ {value: 'public', label: publicLabel }, {value: 'private', label: privateLabel}] }></ChooseField>

                {
                    type == 'text' ? (
                        <ContentTextField label="Text Content" labelIcon="book" value={ content as string } changeHandler={ setContent }/>
                    ): null
                }

                {
                    type == 'form' ? (
                        <ContentQuizField label="Form content" labelIcon="book" defaultValue={ content as Quiz} changeHandler={ () => setContent }/>
                    ): null
                }

                <div className="button-group">
                    <button type="submit">{ saveContentLabel }</button>
                </div>
            </form>
        </>

    );
}