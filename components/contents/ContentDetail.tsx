import { Content, Question } from '@/interfaces/index';
import styles from '@/styles/Content.module.scss';
import React from 'react';
import MarkdownPreview from '@uiw/react-markdown-preview'
import ContentVisibilityBadge from './elements/ContentVisibilityBadge';
import { localeDate } from '@/utils/date';

type ContentDetailHeaderProps = {
    content: Content
}

const ContentDetailHeader = ({content}: ContentDetailHeaderProps) => {

    return (
        <>
            <header className={styles.header}>
                <ContentVisibilityBadge isPublic={ content.isPublic }/>
                <h1>{ content.title }</h1>
                <p>{ content.created_at ? localeDate(content.created_at): null }</p>
            </header>
            <blockquote>
                <p>{ content.description }</p>
            </blockquote>
        </>
    )
}

export const TextContentDetail = ({content}: ContentDetailHeaderProps) => {

    return (
        <div className={styles.detail}>
            <ContentDetailHeader content={content}/>
            <div className={ styles.textContent}>
                <MarkdownPreview source={ content.content as string || "" }/>
            </div>
        </div>
    )
}




export const QuizContentDetail = ({content}: ContentDetailHeaderProps) => {

    return (
        <div className={styles.detail}>
            <ContentDetailHeader content={content}/>
            <div className={ styles.quizContent}>
                {

                    (content.content as Question[]).map((question: Question, questionIndex: number) => (
                        <article>
                            <p>{question.question}</p>
                            { 
                                question.answers.map((answer: string, answerIndex: number) => (
                                    <div>
                                        <label>
                                            <input type="radio" name={ "q" + questionIndex } value={answerIndex}/>
                                            {answer}
                                        </label>

                                    </div>
                                ))
                            }
                        </article>


                    ))
                }
            </div>
        </div>
    )
}