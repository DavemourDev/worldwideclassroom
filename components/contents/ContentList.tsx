import { Content, User } from "@/interfaces/index"
import Link from "next/link"
import React from "react"
import styles from '@/styles/Content.module.scss';
import useDictionary from "../hooks/use-dictionary";

type ContentListItemProps = {
    content: Content;
    showExcerpt?: boolean;
    showAuthor?: boolean;
}

type ContentListProps = {
    contents: Content[];
    showExcerpt?: boolean;
    showAuthor?: boolean;
}

export const ContentList = ({ contents, showExcerpt=false, showAuthor=false }: ContentListProps) => {
    
    const {noContentsMessage} = useDictionary();
    
    if (contents.length > 0) {
        return (
            <div className={ styles.contentList } >
                { contents.map(c => <ContentListItem 
                    content={ c } 
                    key={ c.id } 
                    showExcerpt={ showExcerpt } 
                    showAuthor={ showAuthor}/> )}
            </div>
        );
    } else {
        return <p>{ noContentsMessage }</p>;
    }
};
export const ContentListItem = ({content, showExcerpt=false, showAuthor=false} : ContentListItemProps) => {


    const {createdBy, textContentLabel, formContentLabel} = useDictionary();

    return (
        <Link href={`/contents/${content.id}`}>
            <div className={ styles.contentListItem } title={content.description}>
                <hgroup>
                    <h3>{ content.title }</h3>
                    <h4>{ content.type == 'text' ? textContentLabel : formContentLabel}</h4>
                </hgroup>
                { showAuthor && <p>{ createdBy }: { (content.author as User).fullName }</p> }
                { showExcerpt && <p>{ content.description }</p> }
                
            </div>
        </Link>
    )


}