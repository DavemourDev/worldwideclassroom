import { User } from "@/interfaces/index"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React from "react"
import styles from '@/styles/UserPanel.module.scss';
import useDictionary from "../hooks/use-dictionary";
import { useAuth } from "../hooks/auth-context";
import { useRouter } from "next/router";
import { useToast } from "../hooks/toast-context";

type UserPanelHeaderProps = {
    user: User
}

export const UserPanelHeader = ({ user }: UserPanelHeaderProps) => {

    const { emptyDescription, teacherLabel, studentLabel, logoutLabel } = useDictionary();
    const {showToast} = useToast();

    const { showDescriptionForm, logout } = useAuth();
    const router = useRouter();

    return (
        <header>
            <figure>
                <img src={user.picture} alt="User avatar"/>
            </figure>
            <div className={ styles.info }>
                <hgroup>
                    <h1>{ user.fullName }</h1>
                    <h2>{ user.role == "teacher" ? teacherLabel : studentLabel }</h2>
                </hgroup>
                <article>
                    <blockquote>
                        { user.description ? <p>{ user.description }</p> : <p className={styles.emptyDescription}>{ emptyDescription }</p>}
                    </blockquote>
                    <div className={ styles.buttonGroup}>
                        <button aria-label="Edit description" title={"Edit description"} onClick={ () => {
                            showDescriptionForm(user.id as string, user.description || '', () => {
                                showToast("success", "Description successfully changed")    
                            });
                        } }>
                            <FontAwesomeIcon icon="pencil-alt"/>
                        </button>
                    </div>
                    <div className={ styles.buttonGroup }>
                        <button onClick={ () => {
                            logout();
                            router.push('/', undefined, {shallow: true})
                        } }>{ logoutLabel }</button>
                    </div>
                </article>
            </div>
        </header>
    );
}
