import React, { useState } from "react";
import TextField from "../forms/TextField";
import styles from '@/styles/Form.module.scss';
import useDictionary from "../hooks/use-dictionary";

type LoginFormProps = {
    onSubmit: (username: string, password: string) => void
}

export const LoginForm = ({ onSubmit }: LoginFormProps) => {

    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const { loginNavLabel, usernameLabel, passwordLabel } = useDictionary();

    return (
        <>
            <h2>{ loginNavLabel }</h2>
            <form onSubmit={(ev) => {
                ev.preventDefault();
                onSubmit(username, password);
                // TODO: Cerrar
            }}>
                <fieldset className={ styles.fieldset}>
                    <TextField label={ usernameLabel } labelIcon={ 'user-circle' } changeHandler={ setUsername } />
                    <TextField label={ passwordLabel } labelIcon={ 'lock' } changeHandler={ setPassword } secret />
                    <div className="button-group">
                        <button type="submit" disabled={ !username || !password }>{ loginNavLabel }</button>
                    </div>
                </fieldset>
            </form>
        </>
    );
}