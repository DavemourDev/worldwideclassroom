import React, { useState } from "react";
import ChooseField from "../forms/ChooseField";
import FileField from "../forms/FileField";
import TextField from "../forms/TextField";
import styles from '@/styles/Form.module.scss';
import { encodeBase64 } from "@/utils/files";
import useDictionary from "../hooks/use-dictionary";

type RoleType = 'teacher' | 'student' ;

type RegisterFormProps = {
    onSubmit: (username: string, password: string, fullName: string, picture: string, role: RoleType) => void
}

export const RegisterForm = ({ onSubmit }: RegisterFormProps) => {

    const [ username, setUsername ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ fullName, setFullName ] = useState(""); 
    const [ role, setRole ] = useState("teacher");
    const [ picture, setPicture] = useState<File | null>(null);

    const { teacherLabel, studentLabel, registerNavLabel, usernameLabel, passwordLabel, roleLabel, fullNameLabel } = useDictionary();

    return (
        <>
            <h2>{ registerNavLabel }</h2>
            <form onSubmit={(ev) => {
                ev.preventDefault();
                console.log({ picture });
                if (picture != null) {
                    // TODO: Convertir a promesa?
                    encodeBase64(picture, (base64Result) => {
                        onSubmit(username, password, fullName, base64Result as string, role as RoleType);
                    });
                }
            }}>
                <fieldset className={ styles.fieldset }>
                    <TextField label={ usernameLabel} labelIcon={ 'user-circle' } changeHandler={ setUsername } />
                    <TextField label={ fullNameLabel} labelIcon={ 'address-card' } changeHandler={ setFullName } />
                    <TextField label={ passwordLabel} labelIcon={ 'lock' } changeHandler={ setPassword } secret />
                    <ChooseField label={ roleLabel } labelIcon={ 'star' } changeHandler={ setRole } 
                        choices={ [{label: teacherLabel, value:'teacher'}, {label: studentLabel, value:'student'}] }/>
                    <FileField label="Avatar" labelIcon={ 'portrait' } changeHandler={ setPicture } extensions={ ['.png', '.jpeg'] }/>
                    <div className="button-group">
                        <button type="submit" disabled={ !username || !password }>{ registerNavLabel }</button>
                    </div>
                </fieldset>
            </form>
        </>
    );
}