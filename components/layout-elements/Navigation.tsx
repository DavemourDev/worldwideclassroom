import Link from 'next/link'
import styles from '@/styles/Navigation.module.scss'
import { useAuth } from '../hooks/auth-context';
import React from 'react';
import useDictionary from '../hooks/use-dictionary';
import { useRouter } from 'next/router';

type NavElementProps = {
    href: string,
    label: string,
    onClick?: () => void
}

const NavElement = ({ href, label,  onClick=() => {}}: NavElementProps) => (
    <div className={ styles.navElement} onClick={ onClick }>
        <Link  href={ href } shallow>
            <a className={ styles.navLink} >{ label }</a>
        </Link>
    </div>
);

export const NavigationBar = () => {
    

    const { showLoginForm, loggedInUser, showRegisterForm } = useAuth();    
    const { aboutNavLabel, exploreNavLabel, loginNavLabel, registerNavLabel } = useDictionary();
    const router = useRouter();

    return (
        <nav className={styles.navBar}>
            <NavElement label={aboutNavLabel} href={'/about'} />
            <NavElement label={exploreNavLabel} href={'/explore'}/>
    { loggedInUser != null ?  (
        <div className={ styles.navElement}>
            <Link href="/" shallow>
                <a className={styles.navLink}>
                    <div className={ styles.userNavElement}>
                        <figure>
                            <img src={ loggedInUser.picture}/>
                        </figure>
                        <p>{ loggedInUser.username }</p>
                    </div>
                </a>
            </Link>
        </div>
     ) : (
         <>
            <div className={ styles.navElement} onClick={ () => showLoginForm(() => {
                    router.push('/user', undefined, { shallow: true});
            }) }>
                <a className={ styles.navLink }>{ loginNavLabel }</a>
            </div>
            <div className={ styles.navElement} onClick={ () => showRegisterForm(() => {

            }) }>
                <a className={ styles.navLink }>{ registerNavLabel }</a>
            </div>
        </>
        )}
    </nav> 
    )
};


