import styles from '@/styles/Logo.module.scss'
import { classNames } from '@/utils/style'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


type LogoProps = {
    size: number
}

export const Logo = ({ size }: LogoProps) => {

    return (
        <div className={ styles.logo} >
            <div className={ styles.cube } style={ {
            width: size,
            height: size
        } }>
                <div className={ classNames(styles.cubeFace, styles.front ) }><FontAwesomeIcon icon={["fas", "abacus"]}/></div>
                <div className={ classNames(styles.cubeFace, styles.back) }><FontAwesomeIcon icon={["fas", "code"]}/></div>
                <div className={ classNames(styles.cubeFace, styles.top ) }><FontAwesomeIcon icon={["fas", "university"]}/></div>
                <div className={ classNames(styles.cubeFace, styles.bottom ) }><FontAwesomeIcon icon={["fas", "user-friends"]}/></div>
                <div className={ classNames(styles.cubeFace, styles.left ) }><FontAwesomeIcon icon={["fas", "flask-potion"]}/></div>
                <div className={ classNames(styles.cubeFace, styles.right ) }><FontAwesomeIcon icon={["fas", "mortar-pestle"]}/></div>
            </div>
        </div>
    )

} 