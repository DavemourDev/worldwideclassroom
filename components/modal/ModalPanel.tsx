import React, { ReactNode } from "react"
import { Modal } from "./Modal"


type ModalPanelProps = {
    children: ReactNode,
    onClose: () => void,
    opened: boolean
}

export const ModalPanel = ({ children, onClose, opened }: ModalPanelProps) => {
    return (
        <Modal onClose={ onClose } opened={ opened }>
            <div className="panel">{ children }</div>   
        </Modal>
    );
}