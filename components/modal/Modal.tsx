import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, {  ReactNode, useEffect, useState } from "react"
import ReactDOM from "react-dom";
import styles from '@/styles/Modal.module.scss';

type ModalProps = {
    children: ReactNode,
    onClose: () => void,
    opened: boolean
}

export const Modal = ({children, onClose, opened}: ModalProps) => {

    const [isBrowser, setIsBrowser] = useState(false);
    const [ closed, setClosed] = useState(!opened);

    useEffect(() => {
        setIsBrowser(true);
        setClosed(false);
    }, []);

    const closeHandler = () => {
        setClosed(true);
        onClose();
    }

    return (
        isBrowser && opened ? ReactDOM.createPortal(
            <div className={ styles.modal + " " + (closed ? styles.closed : "") }>
                <div className={ styles.backdrop} onClick={ () => {} }></div>
                <div className={ styles.content }>
                    <div className={ styles.closeButton} aria-label="Close modal" role="button" onClick={ closeHandler }>
                        <FontAwesomeIcon icon="window-close" scale="2"/> 
                    </div>
                    { children }
                </div>
            </div>
        , document.getElementById("modal-root") as Element) : null
    );

};