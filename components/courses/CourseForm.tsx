import { useState } from "react";
import TextField from "../forms/TextField";
import useDictionary from "../hooks/use-dictionary";

// TODO: Submit handler tiene que recibir como parámetro una instancia de Content
type CourseFormProps = {
    author: string,
    submitHandler: (course: any) => void
}

export const CourseForm = ({ submitHandler, author}: CourseFormProps ) => {
    const [ title, setTitle ] = useState("");
    const [ description, setDescription ] = useState("");

    const { createCourseLabel, saveCourseLabel, titleLabel, descriptionLabel} = useDictionary();

    return (
        <>
            <h2>{ createCourseLabel }</h2>
            <form onSubmit={ (ev) => {
                    ev.preventDefault();
                    submitHandler({
                        title, description, author
                    });
                }
            }>
                <TextField label={ titleLabel} labelIcon="heading" changeHandler={ setTitle }/>
                <TextField label={ descriptionLabel} labelIcon="text" changeHandler={ setDescription } long/>
                <div className="button-group">
                    <button type="submit">{ saveCourseLabel }</button>
                </div>
            </form>
        </>

    );
}