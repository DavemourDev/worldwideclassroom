import { Course } from "@/interfaces/index"
import Link from "next/link"
import React from "react"
import styles from '@/styles/Content.module.scss';
import useDictionary from "../hooks/use-dictionary";

type CourseListItemProps = {
    course: Course,
    showExcerpt?: boolean,
    clickHandler?: ((course: Course) => void) | null
}

type CourseListProps = {
    courses: Course[];
    onClickCourse?: ((course: Course) => void) | null
};

export const CourseList = ({ courses, onClickCourse=null }: CourseListProps) => {
    
    const { noCoursesMessage } = useDictionary(); 

    if (courses.length > 0) {
        return (
            <div className={ styles.courseList}>
                { courses.map(c => (
                    <CourseListItem course={ c } clickHandler={ (_ev) => {
                        if (onClickCourse != null) {
                            onClickCourse(c);
                        }
                    }}/>
                )) }
            </div>
        );
    } else {
        return <p>{ noCoursesMessage }</p>;
    }
}

export const CourseListItem = ({course,  clickHandler=null } : CourseListItemProps) => {

    return (
        <Link href={`./courses/${course.id}`} >
            <div className={ styles.courseListItem } onClick={ (ev) => {
                if (clickHandler != null) {
                    ev.preventDefault();
                    clickHandler(course);
                }
            }} title={ course.description }>
                <hgroup>
                    <h3>{ course.title }</h3>
                    <h4>{ course.subject }</h4>
                </hgroup>
                
            </div>
        </Link>
    )


}