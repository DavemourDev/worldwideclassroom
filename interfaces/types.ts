export type PictureExtension = ".jpg" | ".jpeg" | ".png" | ".gif";

export type VideoExtension = "";

export type TextContentExtension = ".txt" | ".md";
