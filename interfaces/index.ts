export type RoleType = "teacher" | "student";
export type ContentType = "text" | "form";

export type IdType = string;

export type User = {
  id?: IdType;
  username: string;
  password?: string;
  picture: string;
  fullName: string;
  description?: string;
  role: RoleType;
};

export type Content = {
  id?: IdType;
  title: string;
  type: ContentType;
  isPublic: boolean;
  author?: string | User;
  created_at?: number;
  description?: string;
  content?: string | Quiz;
};

export type Course = {
  id?: IdType;
  title: string;
  subject?: string;
  description: string;
  author: string | User;
  created_at: number;
};

export type Question = {
  question: string;
  answers: string[];
  correct: number;
};

export type Quiz = Question[];
