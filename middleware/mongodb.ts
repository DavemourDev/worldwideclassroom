import dbConnect from "@/utils/database";
import { NextApiRequest, NextApiResponse } from "next";

type HandlerType = (req: NextApiRequest, res: NextApiResponse) => void;

export default (handler: HandlerType) =>
  async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      await dbConnect();
      return handler(req, res);
    } catch (error) {}
  };
